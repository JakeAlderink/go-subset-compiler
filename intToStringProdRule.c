#include "main.h"
#include "productionRules.h"

/*simple number to string printer */
void printIntToStringProdRule(int prodruleInt)
{
  switch(prodruleInt)
    {
    case fnbody:
      printf("%s","fnbody");
      break;
    case elsesss:
      printf("%s","elsesss");
      break;
    case elseif_list:
      printf("%s","elseif_list");
      break;
    case elseif_stmt:
      printf("%s","elseif_stmt");
      break;
    case imports:
      printf("%s","imports");
      break;
    case file:
      printf("%s","file");
      break;
    case package:
      printf("%s","package");
      break;
    case import:
      printf("%s","import");
      break;
    case import_stmt:
      printf("%s","import_stmt");
      break;
    case import_stmt_list:
      printf("%s","import_stmt_list");
      break;
    case import_here:
      printf("%s","import_here");
      break;
    case import_package:
      printf("%s","import_package");
      break;
    case import_there:
      printf("%s","import_there");
      break;
    case lconst:
      printf("%s","lconst");
      break;
    case common_dcl:
      printf("%s","common_dcl");
      break;
    case vardcl:
      printf("%s","vardcl");
      break;
    case constdcl:
      printf("%s","constdcl");
      break;
    case constdcl1:
      printf("%s","constdcl1");
      break;
    case typedclname:
      printf("%s","typedclname");
      break;
    case typedcl:
      printf("%s","typedcl");
      break;
    case simple_stmt:
      printf("%s","simple_stmt");
      break;
    case compound_stmt:
      printf("%s","compound_stmt");
      break;
    case loop_body:
      printf("%s","loop_body");
      break;
    case range_stmt:
      printf("%s","range_stmt");
      break;
    case for_header:
      printf("%s","for_header");
      break;
    case for_body:
      printf("%s","for_body");
      break;
    case for_stmt:
      printf("%s","for_stmt");
      break;
    case if_header:
      printf("%s","if_header");
      break;
    case if_stmt:
      printf("%s","if_stmt");
      break;
    case elseif:
      printf("%s","elseif");
      break;
    case switch_stmt:
      printf("%s","switch_stmt");
      break;
    case select_stmt:
      printf("%s","select_stmt");
      break;
    case expr:
      printf("%s","expr");
      break;
    case uexpr:
      printf("%s","uexpr");
      break;
    case pseudocall:
      printf("%s","psuedocall");
      break;
    case pexpr_no_paren:
      printf("%s","pexpr_no_paren");
      break;
    case start_complit:
      printf("%s","start_complit");
      break;
    case keyval:
      printf("%s","keyval");
      break;
    case bare_complitexpr:
      printf("%s","bare_complitexpr");
      break;
    case complitexpr:
      printf("%s","complitexpr");
      break;
    case pexpr:
      printf("%s","pexpr");
      break;
    case expr_or_type:
      printf("%s","expr_or_type");
      break;
    case name_or_type:
      printf("%s","name_or_type");
      break;
    case lbrace:
      printf("%s","lbrace");
      break;
    case new_name:
      printf("%s","new_name");
      break;
    case dcl_name:
      printf("%s","dcl_name");
      break;
    case sym:
      printf("%s","sym");
      break;
    case hidden_importsym:
      printf("%s","hidden_importsym");
      break;
    case nameee:
      printf("%s","name");
      break;
    case labelname:
      printf("%s","labelname");
      break;
    case dotdotdot:
      printf("%s","dotdotdot");
      break;
    case ntype:
      printf("%s","ntype");
      break;
    case non_expr_type:
      printf("%s","non_expr_type");
      break;
    case non_recvchantype:
      printf("%s","non_recvchantype");
      break;
    case convtype:
      printf("%s","convtype");
      break;
    case comptype:
      printf("%s","comptype");
      break;
    case fnret_type:
      printf("%s","fnret_type");
      break;
    case dotname:
      printf("%s","dotname");
      break;
    case othertype:
      printf("%s","othertype");
      break;
    case ptrtype:
      printf("%s","ptrtype");
      break;
    case recvchantype:
      printf("%s","recvchantype");
      break;
    case structtype:
      printf("%s","structtype");
      break;
    case interfacetype:
      printf("%s","interfacetype");
      break;
    case xfndcl:
      printf("%s","xfndcl");
      break;
    case fndcl:
      printf("%s","fndcl");
      break;
    case hidden_fndcl:
      printf("%s","hidden_fndcl");
      break;
    case fntype:
      printf("%s","fntype");
      break;
    case fnres:
      printf("%s","fnres");
      break;
    case fnlitdcl:
      printf("%s","fnlitdcl");
      break;
    case fnliteral:
      printf("%s","fnliteral");
      break;
    case xdcl_list:
      printf("%s","xdcl_list");
      break;
    case vardcl_list:
      printf("%s","vardcl_list");
      break;
    case constdcl_list:
      printf("%s","constdcl_list");
      break;
    case typedcl_list:
      printf("%s","typedcl_list");
      break;
    case structdcl_list:
      printf("%s","structdcl_list");
      break;
    case interfacedcl_list:
      printf("%s","interfacedcl_list");
      break;
    case structdcl:
      printf("%s","structdcl");
      break;
    case packname:
      printf("%s","packname");
      break;
    case embed:
      printf("%s","embed");
      break;
    case interfacedcl:
      printf("%s","interfacedcl");
      break;
    case indcl:
      printf("%s","indcl");
      break;
    case arg_type:
      printf("%s","arg_type");
      break;
    case arg_type_list:
      printf("%s","arg_type_list");
      break;
    case non_dcl_stmt:
      printf("%s","non_dcl_stmt");
      break;
    case stmt_list:
      printf("%s","stmt_list");
      break;
    case new_name_list:
      printf("%s","new_name_list");
      break;
    case dcl_name_list:
      printf("%s","dcl_name_list");
      break;
    case expr_list:
      printf("%s","expr_list");
      break;
    case expr_or_type_list:
      printf("%s","expr_or_type_list");
      break;
    case keyval_list:
      printf("%s","keyval_list");
      break;
    case hidden_import:
      printf("%s","hidden_import");
      break;
    case hidden_pkg_importsym:
      printf("%s","hidden_pkg_importsym");
      break;
    case hidden_pkgtype:
      printf("%s","hidden_pkgtype");
      break;
    case hidden_type:
      printf("%s","hidden_type");
      break;
    case hidden_type_non_recv_chan:
      printf("%s","hidden_type_non_recv_chan");
      break;
    case hidden_type_misc:
      printf("%s","hidden_type_misc");
      break;
    case hidden_type_recv_chan:
      printf("%s","hidden_type_recv_chan");
      break;
    case hidden_type_func:
      printf("%s","hidden_type_func");
      break;
    case hidden_funarg:
      printf("%s","hidden_funarg");
      break;
    case hidden_structdcl:
      printf("%s","hidden_structdcl");
      break;
    case hidden_interfacedcl:
      printf("%s","hidden_interfacedcl");
      break;
    case hidden_funres:
      printf("%s","hidden_funres");
      break;
    case hidden_literal:
      printf("%s","hidden_literal");
      break;
    case hidden_constant:
      printf("%s","hidden_constant");
      break;
    case hidden_funarg_list:
      printf("%s","hidden_funarg_list");
      break;
    case hidden_structdcl_list:
      printf("%s","hidden_structdcl_list");
      break;
    case hidden_interfacedcl_list:
      printf("%s","hidden_interfacescl_list");
      break;
    case oarg_type_list_ocomma:
      printf("%s","oarg_type_list_ocomma");
      break;
    case braced_keyval_list:
      printf("%s","braced_keyval_list");
      break;
    case hidden_import_list:
      printf("%s","hidden_import_list");
      break;
    }
  
}
