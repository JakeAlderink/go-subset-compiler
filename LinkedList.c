#include "LinkedList.h"

void printFormattedScopeList()
{
  struct ScopeList* temp = ScopeListHead;
  while(temp)
    {
      printHashtable(temp);
      temp = temp->next;
    }
}

void printScopesParameters()
{
  struct ScopeList* temp = ScopeListHead;
  while(temp)
    {
      printParameters(temp);
      temp = temp->next;
    }
}


/*add to tree LinkedList*/
int addToLinkedList()
{
  struct TreeList* tmp = NULL;
  if(!TreeListHead)
    {
      TreeListHead = (struct TreeList*)malloc(sizeof(struct TreeList));
      TreeListHead->next = NULL;
      TreeListHead->treeHead = head;
      
      return 1;
    }
  else
    {
      tmp = TreeListHead;
      while(tmp->next != NULL)
	tmp = tmp->next;
      tmp->next = (struct TreeList*)malloc(sizeof(struct TreeList));
      tmp->next->next = NULL;
      tmp->next->treeHead = head;
      return 1;
    }
  return -1;
}


/*primt the trees in the linked list*/
int printFormattedLinkedList()  /* prints out the formated output for the linked list for homework 1 */
{
  struct TreeList* tmp = TreeListHead;
  while(tmp != NULL)
    {
      printTree(tmp->treeHead);
      tmp=tmp->next;
    }
  return 1;  
}


/*adds global scope to the scope linked list */
struct ScopeList* ScopeInit()
{
  int i;
  ScopeListHead = (struct ScopeList*)malloc(sizeof(struct ScopeList));
  ScopeListHead->next = NULL;
  ScopeListHead->scopeName = (char*)malloc(strlen("Global Scope")+1);
  ScopeListHead->structType = 0;
  strcpy(ScopeListHead->scopeName,"Global Scope");
  /*ScopeListHead->tableVal = {NULL};*/
  
  for(i = 0; i < Size; i++)
    {
      ScopeListHead->tableVal[i] = NULL;
      //Set All Values in hash table to NULL
    }
  ScopeListHead->head = NULL;
  return ScopeListHead;
}

/* add new scope to scope list*/
struct ScopeList* addNewScope(char* scopeName, int structType)
{
  int i = 0;
  if(!ScopeListHead)
    {
      printf("Error Global Scope Not defined\n");
      exit(3);
    }
  else
    {
      struct ScopeList* tmp = ScopeListHead;
      while(tmp->next != NULL)
	{
	  tmp = tmp->next;
	}
      tmp->next  = (struct ScopeList*)malloc(sizeof(struct ScopeList));
      tmp = tmp->next;
      tmp->next = NULL;
      tmp->scopeName = strdup(scopeName);
      tmp->structType = structType;
      /*strcpy(tmp->scopeName,scopeName);*/
      for(i = 0; i < Size; i++)
	{
	  tmp->tableVal[i] = NULL;
	  //Set All Values in hash table to NULL
	}
      tmp->head = NULL;
      return tmp;
    }
  return NULL;
}
