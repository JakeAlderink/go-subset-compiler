#ifndef HASHT_H
#define HASHT_H

#define Size 100
#include "main.h"
#include "LinkedList.h"

/*
  Single Hash table entry
  Collisions are resolved via a linked List
 */
struct tokenHashList{
  struct tokenHashList* next;
  int datatype;
  /*
    0 int
    1 float
    2 string
    3 bool
    4 other
  */
  char* key;
  int auxFlags; /*binary number representing certain info*/
  /*
  0 0 0 0 0 0 0-is const
  | | | | | |
  | | | | | L--is map
  | | | | |
  | | | | L---is pointer
  | | | |
  | | | is function 
  | | |
  | | is parameter
  | |
  | was passed in via import  
  |
  is array

  */

  int mapType;
  /*Map type that is used for the map hash */
  
  
  int arraySize;
  /*if an array how many elements within the array exist*/
  int byteSize;
  int offset;
};


/*
  wrapper for the tokenHashList represents the Hast Table
  typically initiaed as: 

  struct table* hashTable[Size];
  One Hash table represent 1 "scope" in semantic analysis
 */
struct table{
  struct tokenHashList* head;
};


/*
Wrapper for tokenHashList for adding PARAMTER Values to a Linked List
NOTE: ALL memory Should already be allocated for the individuals;
Now just adding to a DATA Stucture
*/

struct ParameterList{
  struct tokenHashList* value;
  struct ParameterList* next;
};


/*
  Add Paramter value
*/
struct ParameterList* addParameterList(struct tokenHashList* value,struct ScopeList* scope);

/*
  Adds a new Hash Value to an existing HashTable(Scope)
 */
struct tokenHashList* addToTable(struct ScopeList* scope, struct token* value, int type, int flags,char* filename,int mapType);



/*
  Adds new Hash to global from import
 */
struct tokenHashList* addToGlobal(struct ScopeList* scope, char* text, int type, int flags, char* filename);

/*
  Removes a Hash Value from an exitsting HashTable(Scope)
*/
int removeFromTable(struct table* scope[Size], struct token* value);


/*
 *Simple check
 */
struct tokenHashList* searchTable(struct table* scope[Size], char*key);


/*Very simple Hash function
  Adds all character asccii values together
  multiplies by a large number
  and mods by Size;
*/
int HashFunction(char*);

void printHashtable(struct ScopeList* scope);
/*
  prints a single hashTable
 */


void printParameters(struct ScopeList* tmp);
/*
  print parameters of a scope
*/


void printTypeHelper(int t);
/*
  Helper Function
*/
#endif 
