// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file
// (a copy is at www2.cs.uidaho.edu/~jeffery/courses/445/go/LICENSE).

/*
 * Go language grammar adapted from Go 1.2.2.
 *
 * The Go semicolon rules are:
 *
 *  1. all statements and declarations are terminated by semicolons.
 *  2. semicolons can be omitted before a closing ) or }.
 *  3. semicolons are inserted by the lexer before a newline
 *      following a specific list of tokens.
 *
 * Rules #1 and #2 are accomplished by writing the lists as
 * semicolon-separated lists with an optional trailing semicolon.
 * Rule #3 is implemented in yylex.
 */

%{
#include "main.h"
#include "tree.h"
#include "productionRules.h"
%}

/*
 * %union declares of what kinds of values appear on the value stack
 */
%union {
  struct tokenTree* node;
}

/*
 * each token is declared.  tokens store leaf values on the value stack
 *
 * Back in javalex.l, we put things on the stack by assigning to yylval

 "if"           { yylval.node = alcnode(IF, 0); return IF; }

 *
 */
%token < node > LBOOLEAN LVAR LCONST LSTRING LINT LFLOAT LLIST LTABLE LRUNE 
%token < node > LLITERAL LCHAN LILITERAL LSLITERAL LBLITERAL LRLITERAL LIMLITERAL LFLITERAL
%token < node > LBREAK LCASE LDEFAULT LCONTINUE LFALL LGOTO LFOR LRETURN LSWITCH
%token < node > LIF LELSE
%token < node > LTYPE LDEFER LFUNC LGO LIMPORT LINTERFACE LMAP LNAME LPACKAGE LRANGE LSELECT LSTRUCT LIDENTIFIER
%token < node > LBAD_TOKEN LIGNORE LNIL
%token < node > LASOP LCOLAS LDDD LANDAND LANDNOT LBODY LCOMM LDEC LEQ LGE LGT LINC LLE LLSH LLT LNE LOROR LRSH LANDEQ
%token < node > LOR LOREQ LNOT LNOTEQ LAND LSTAREQ LDIVEQ LLLE LMODEQ LRRE LCOLON LANDNOTEQ LSTAR LDIVIDE LMOD LMINUS
%token < node > LPLUS LLPAR LRPAR LLBRK LRBRK LEXC LPLUSEQ LMINUSEQ LLBRA LRBRA LSM LCM LDOT LCURLY LAT LQUEST LDOLLAR 


%type < node > lconst package packname
%type < node > lbrace import_here   


%type < node >		oliteral fnlitdcl
/*%type < node >		hidden_import*/

%type	< node >	stmt ntype sym  import_package 
%type   < node >	arg_type
/*%type	< node >	case caseblock hidden_fndcl import_there*/
%type	< node >	compound_stmt dotname embed expr complitexpr bare_complitexpr file
%type	< node >	expr_or_type
%type	< node >	fndcl  fnliteral
%type	< node >	for_body for_header for_stmt if_header if_stmt non_dcl_stmt
%type	< node >	keyval labelname name
%type	< node >	name_or_type non_expr_type
%type	< node >	new_name dcl_name oexpr typedclname
%type	< node >	onew_name
%type	< node >	osimple_stmt pexpr pexpr_no_paren
%type	< node >	pseudocall  
%type	< node >	simple_stmt
%type	< node >	 uexpr
%type	< node >	xfndcl typedcl start_complit

%type < node >		xdcl fnbody fnres loop_body dcl_name_list
%type < node >		new_name_list expr_list keyval_list braced_keyval_list expr_or_type_list xdcl_list
%type	< node >	oexpr_list elseif elseif_list else stmt_list oarg_type_list_ocomma arg_type_list
%type	< node >	vardcl vardcl_list structdcl structdcl_list
%type	< node >	common_dcl constdcl constdcl1 constdcl_list typedcl_list

%type	< node >	convtype
/*%type	< node >        comptype */
%type	< node >	structtype ptrtype
%type	< node >	othertype fnret_type fntype

%type	< node >	 imports import_safety

/*%type	< node >	hidden_constant hidden_literal hidden_funarg hidden_pkg_importsym
%type	< node >	hidden_structdcl
*/
/*
%type	< node >	hidden_funres
%type	< node >	ohidden_funres
%type	< node >	hidden_funarg_list ohidden_funarg_list
/*%type	< node >	hidden_interfacedcl_list ohidden_interfacedcl_list
%type	< node >	hidden_structdcl_list ohidden_structdcl_list
*/

/*%type	< node >	hidden_type hidden_type_misc hidden_pkgtype*/
/*%type	< node >	hidden_type_func hidden_import_list*/
%type	< node >	osemi ocomma  import import_stmt import_stmt_list
									    


%left		LCOMM	/* outside the usual hierarchy; here for good error messages */

%left		LOROR
%left		LANDAND
%left		LNE LLE LGE LLT LGT LEQ LMINUSEQ LPLUSEQ
%left		LPLUS LMINUS LOR LNOT 
%left		LSTAR LDIVIDE LMOD LAND LLSH LRSH LANDNOT 

%left		NotPackage
%left		LPACKAGE

%left		NotParen
%left		LLPAR

%left		LRPAR
%left		PreferToRightParen



/*
 * manual override of shift/reduce conflicts.
 * the general form is that we assign a precedence
 * to the token being shifted and then introduce
 * NotToken with lower precedence or PreferToToken with higher
 * and annotate the reducing rule accordingly.
 */ 






%error-verbose

%%
file:	package	imports	xdcl_list {$$=AllocateTreeProductionNode(file,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;
				   head = $$;}
				   ;

package:
	%empty
	%prec NotPackage 
	{
	  ayyerror("package statement must be first",2);
		exit(1);
	}
|	LPACKAGE sym LSM {$$=AllocateTreeProductionNode(package,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}
        ;

imports:
        %empty{$$=NULL;}
|	imports import LSM{$$=AllocateTreeProductionNode(imports,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}

        ;

import:
	LIMPORT import_stmt{$$=AllocateTreeProductionNode(import,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;}

|	LIMPORT LLPAR import_stmt_list osemi LRPAR{$$=AllocateTreeProductionNode(import,5);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;
				   $$->branch[3] = $4;
				   $$->branch[4] = $5;}

|	LIMPORT LLPAR LRPAR{$$=AllocateTreeProductionNode(import,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}
	;

import_stmt:
        import_here{$$=$1;}
|	import_here import_package{$$=AllocateTreeProductionNode(import_stmt,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;		}

	;

import_stmt_list:
	import_stmt{$$=$1;}
|	import_stmt_list LSM import_stmt{$$=AllocateTreeProductionNode(import_stmt_list,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}

	;

import_here:
	LLITERAL{$$=$1;}
|	sym LLITERAL{$$=AllocateTreeProductionNode(import_here,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;		}

|	LDOT LLITERAL{$$=AllocateTreeProductionNode(import_here,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;		}

	;

import_package:
	LPACKAGE LIDENTIFIER import_safety LSM{$$=AllocateTreeProductionNode(import_package,4);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

	;

import_safety:
        %empty{$$=NULL;}
|	LIDENTIFIER{$$=$1;}
	;

/*
import_there:
	hidden_import_list LDOLLAR LDOLLAR{$$=AllocateTreeProductionNode(import_there,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}
*/

xdcl:
        %empty
	{
	  ayyerror("empty top-level declaration",2);
		$$ = NULL;
	}
|	common_dcl{$$ = $1;}
|	xfndcl{$$ = $1;}
|	non_dcl_stmt
	{
	  ayyerror("non-declaration statement outside function body",2);
		$$ = NULL;
	}
	;


lconst:
	LCONST{$$ = $1;}
	;


common_dcl:
	LVAR vardcl{$$=AllocateTreeProductionNode(common_dcl,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;	}

|	LVAR LLPAR vardcl_list osemi LRPAR{$$=AllocateTreeProductionNode(common_dcl,5);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LVAR LLPAR LRPAR{$$=AllocateTreeProductionNode(common_dcl,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}

|	lconst constdcl{$$=AllocateTreeProductionNode(common_dcl,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2; }

|	lconst LLPAR constdcl osemi LRPAR{$$=AllocateTreeProductionNode(common_dcl,5);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	lconst LLPAR constdcl LSM constdcl_list osemi LRPAR{$$=AllocateTreeProductionNode(common_dcl,7);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		
				   $$->branch[5] = $6;		
				   $$->branch[6] = $7;		}

|	lconst LLPAR LRPAR{$$=AllocateTreeProductionNode(common_dcl,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}

|	LTYPE typedcl{$$=AllocateTreeProductionNode(common_dcl,2);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;	}
				   

|	LTYPE LLPAR typedcl_list osemi LRPAR{$$=AllocateTreeProductionNode(common_dcl,5);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LTYPE LLPAR LRPAR{$$=AllocateTreeProductionNode(common_dcl,3);
				   $$->branch[0] = $1;				
				   $$->branch[1] = $2;				
				   $$->branch[2] = $3;		}

	;

vardcl:
	dcl_name_list ntype{$$=AllocateTreeProductionNode(vardcl,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	dcl_name_list ntype LASOP expr_list{$$=AllocateTreeProductionNode(vardcl,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

|	dcl_name_list LASOP expr_list{$$=AllocateTreeProductionNode(vardcl,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

constdcl:
	dcl_name_list ntype LASOP expr_list{$$=AllocateTreeProductionNode(constdcl,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

|	dcl_name_list LASOP expr_list{$$=AllocateTreeProductionNode(constdcl,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

constdcl1:
	constdcl{$$=$1;}
|	dcl_name_list ntype{$$=AllocateTreeProductionNode(constdcl1,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	dcl_name_list{$$=$1;}
	;

typedclname:
	sym
	{
		// the name becomes visible right here, not at the end
		// of the declaration.
		$$=$1;
	}
	;

typedcl:
	typedclname ntype{$$=AllocateTreeProductionNode(typedcl,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;


simple_stmt:
	expr{$$=$1;}
|	expr LASOP expr{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}


|	expr LMINUSEQ expr{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LPLUSEQ expr{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr_list LASOP expr_list{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr_list LMINUSEQ expr_list{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr_list LPLUSEQ expr_list{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr_list LCOLAS expr_list{$$=AllocateTreeProductionNode(simple_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LINC{$$=AllocateTreeProductionNode(simple_stmt,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;}
|	expr LDEC{$$=AllocateTreeProductionNode(simple_stmt,2);
	                           $$->branch[0] = $1;
	                           $$->branch[1] = $2;}
	;




compound_stmt:
	LLBRA
	stmt_list LRBRA{$$=AllocateTreeProductionNode(compound_stmt,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		
				   $$->branch[2] = $3;		}

	;
	
/*
caseblock:
	case
	{
		// If the last token read by the lexer was consumed
		// as part of the case, clear it (parser has cleared yychar).
		// If the last token read by the lexer was the lookahead
		// leave it alone (parser has it cached in yychar).
		// This is so that the stmt_list action doesn't look at
		// the case tokens if the stmt_list is empty.
		//yylast = yychar;
	}
	stmt_list
	{
	  //int last;

		// This is the only place in the language where a statement
		// list is not allowed to drop the final semicolon, because
		// it's the only place where a statement list is not followed 
		// by a closing brace.  Handle the error for pedantry.

		// Find the final token of the statement list.
		// yylast is lookahead; yyprev is last of stmt_list
		//last = yyprev;

		//if(last > 0 && last != LSM && yychar != LRBRA)
		//	yyerror("missing statement after label");
	}


caseblock_list:%empty{$$=NULL;}
|	caseblock_list caseblock
	;
*/


loop_body:
	LLBRA
	stmt_list LRBRA{$$=AllocateTreeProductionNode(loop_body,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		
				   $$->branch[2] = $3;		}

	;

/*
range_stmt:
	expr_list LASOP LRANGE expr
|	expr_list LCOLAS LRANGE expr
	;
*/

for_header:
	osimple_stmt LSM osimple_stmt LSM osimple_stmt{$$=AllocateTreeProductionNode(for_header,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	osimple_stmt{$$=$1;}
	;

for_body:
	for_header loop_body{$$=AllocateTreeProductionNode(for_body,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

for_stmt:
	LFOR
	for_body{$$=AllocateTreeProductionNode(for_stmt,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

if_header:
	osimple_stmt{$$=$1;}
|	osimple_stmt LSM osimple_stmt{$$=AllocateTreeProductionNode(if_header,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

if_stmt:
	LIF
	if_header
	loop_body
	elseif_list else{$$=AllocateTreeProductionNode(if_stmt,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

	;

elseif:
	LELSE LIF 
	if_header loop_body{$$=AllocateTreeProductionNode(elseif,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		
				   $$->branch[2] = $3;		}

	;

elseif_list:
        %empty{$$=NULL;}
|	elseif_list elseif{$$=AllocateTreeProductionNode(elseif_list,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

else:
        %empty{$$=NULL;}
|	LELSE compound_stmt{$$=AllocateTreeProductionNode(elsesss,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

/*
switch_stmt:
	LSWITCH
	if_header
	LBODY caseblock_list LRBRA
	;
*/

/*
select_stmt:
	LSELECT
	LBODY caseblock_list LRBRA
	;
*/

expr:
	uexpr{$$=$1;}
|	expr LOROR expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LANDAND expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LEQ expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LNE expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LLT expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LLE expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LGE expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LGT expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LPLUS expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LMINUS expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LOR expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LNOT expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LSTAR expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LDIVIDE expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LMOD expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LAND expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LANDNOT expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LLSH expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	expr LRSH expr{$$=AllocateTreeProductionNode(expr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}


	;




uexpr:
/*add LINC and LDEC here for precedence purposes*/
	pexpr{$$=$1;}
|	LSTAR uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LAND uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LPLUS uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LMINUS uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LEXC uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LCURLY uexpr
	{
	  ayyerror("the bitwise complement operator is ^",2);
	}
|	LNOT uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LCOMM uexpr{$$=AllocateTreeProductionNode(uexpr,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

pseudocall:
	pexpr LLPAR LRPAR{$$=AllocateTreeProductionNode(pseudocall,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	pexpr LLPAR expr_or_type_list ocomma LRPAR{$$=AllocateTreeProductionNode(pseudocall,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

	;

pexpr_no_paren:
	LLITERAL{$$=$1;}
|	name{$$=$1;}
|	pexpr LDOT sym{$$=AllocateTreeProductionNode(pexpr_no_paren,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	pexpr LDOT LLPAR expr_or_type LRPAR{$$=AllocateTreeProductionNode(pexpr_no_paren,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	pexpr LDOT LLPAR LTYPE LRPAR{$$=AllocateTreeProductionNode(pexpr_no_paren,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	pexpr LLBRK expr LRBRK{$$=AllocateTreeProductionNode(pexpr_no_paren,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

|	pexpr LLBRK oexpr LCOLON oexpr LRBRK{$$=AllocateTreeProductionNode(pexpr_no_paren,6);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;
				   $$->branch[5] = $6;		}

|	pexpr LLBRK oexpr LCOLON oexpr LCOLON oexpr LRBRK{$$=AllocateTreeProductionNode(pexpr_no_paren,8);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;
				   $$->branch[5] = $6;
				   $$->branch[6] = $7;
				   $$->branch[7] = $8;		}

|	pseudocall{$$=$1;}
|	convtype LLPAR expr ocomma LRPAR{$$=AllocateTreeProductionNode(pexpr_no_paren,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LLPAR expr_or_type LRPAR LLBRA start_complit braced_keyval_list LRBRA
	{
	  ayyerror("cannot parenthesize type in composite literal",2);
	}
|	fnliteral{$$=$1;}

/*
|	comptype LLBRA start_complit braced_keyval_list LRBRA{$$=AllocateTreeProductionNode(pexpr_no_paren,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	pexpr_no_paren LLBRA start_complit braced_keyval_list LRBRA{$$=AllocateTreeProductionNode(pexpr_no_paren,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}
*/

;

start_complit:
	%empty{
		// composite expression.
		// make node early so we get the right line number.
		
		$$=NULL;
	}
	;

keyval:
	expr LCOLON complitexpr{$$=AllocateTreeProductionNode(keyval,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

bare_complitexpr:
	expr{$$=$1;}
|	LLBRA start_complit braced_keyval_list LRBRA{$$=AllocateTreeProductionNode(bare_complitexpr,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

	;

complitexpr:
	expr{$$=$1;}
|	LLBRA start_complit braced_keyval_list LRBRA{$$=AllocateTreeProductionNode(complitexpr,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;          }
	;

pexpr:
	pexpr_no_paren{$$=$1;}
|	LLPAR expr_or_type LRPAR{$$=AllocateTreeProductionNode(pexpr,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

expr_or_type:
	expr{$$=$1;}
|	non_expr_type	%prec PreferToRightParen {$$=$1;}
        ;

name_or_type:
	ntype{$$=$1;}
        ;

lbrace:
	LBODY{$$=$1;}
|	LLBRA{$$=$1;}
	;

new_name:
	sym{$$=$1;}
	;

dcl_name:
	sym{$$=$1;}
	;

onew_name:
        %empty{$$=NULL;}
|	new_name{$$=$1;}
	;

sym:
	LIDENTIFIER{$$=$1;}
|	LQUEST{$$=$1;}
;

/*
hidden_importsym:
	LAT LLITERAL LDOT LIDENTIFIER {$$=AllocateTreeProductionNode(hidden_importsym,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

|	LAT LLITERAL LDOT LQUEST{$$=AllocateTreeProductionNode(hidden_importsym,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}
	;
*/

name:
	sym	%prec NotParen{$$=$1;}
	;

labelname:
	new_name{$$=$1;}
	;

ntype:
	fntype{$$=$1;}
|       LINT{$$=$1;}
|       LFLOAT{$$=$1;}
|       LSTRING{$$=$1;}
|       LBOOLEAN{$$=$1;}
|       othertype{$$=$1;}
|	ptrtype{$$=$1;}
|	dotname{$$=$1;}
|	LLPAR ntype LRPAR{$$=AllocateTreeProductionNode(ntype,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;


non_expr_type:
	othertype{$$=$1;}
|	LSTAR non_expr_type{$$=AllocateTreeProductionNode(non_expr_type,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2; }
	;

convtype:
	othertype{$$=$1;}
	;

/*
comptype:
	othertype{$$=$1;}
	;
*/

fnret_type:
	fntype{$$=$1;}
|	othertype{$$=$1;}
|	ptrtype{$$=$1;}
|	dotname{$$=$1;}
|       LINT{$$=$1;}
|       LFLOAT{$$=$1;}
|       LBOOLEAN{$$=$1;}
|       LSTRING{$$=$1;}
;

dotname:
	name{$$=$1;}
|	name LDOT sym{$$=AllocateTreeProductionNode(dotname,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

othertype:
	LLBRK oexpr LRBRK ntype{$$=AllocateTreeProductionNode(othertype,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		}

|	LMAP LLBRK ntype LRBRK ntype{$$=AllocateTreeProductionNode(othertype,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}
|	structtype{$$=$1;}
	;

ptrtype:
	LSTAR ntype{$$=AllocateTreeProductionNode(ptrtype,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

/*
recvchantype:
	LCOMM LCHAN ntype
	;

*/

structtype:
	LSTRUCT lbrace structdcl_list osemi LRBRA{$$=AllocateTreeProductionNode(structtype,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}
|	LSTRUCT lbrace LRBRA{$$=AllocateTreeProductionNode(structtype,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;


xfndcl:
	LFUNC fndcl fnbody{$$=AllocateTreeProductionNode(xfndcl,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

fndcl:
	sym LLPAR oarg_type_list_ocomma LRPAR fnres{$$=AllocateTreeProductionNode(fndcl,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LLPAR oarg_type_list_ocomma LRPAR sym LLPAR oarg_type_list_ocomma LRPAR fnres{$$=AllocateTreeProductionNode(fndcl,8);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		
				   $$->branch[5] = $6;		
				   $$->branch[6] = $7;		
				   $$->branch[7] = $8;		}

	;
/*
hidden_fndcl:
	hidden_pkg_importsym LLPAR ohidden_funarg_list LRPAR ohidden_funres{$$=AllocateTreeProductionNode(hidden_fndcl,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LLPAR hidden_funarg_list LRPAR sym LLPAR ohidden_funarg_list LRPAR ohidden_funres{$$=AllocateTreeProductionNode(hidden_fndcl,8);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		
				   $$->branch[5] = $6;		
				   $$->branch[6] = $7;		
				   $$->branch[7] = $8;		}

	;
*/
fntype:
	LFUNC LLPAR oarg_type_list_ocomma LRPAR fnres{$$=AllocateTreeProductionNode(fntype,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

	;

fnbody:
        %empty{$$=NULL;}
|	LLBRA stmt_list LRBRA{$$=AllocateTreeProductionNode(fnbody,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

fnres:
	%empty{$$=NULL;} %prec NotParen
|	fnret_type{$$=$1;}
	;

fnlitdcl:
	fntype{$$=$1;}
	;

fnliteral:
	fnlitdcl lbrace stmt_list LRBRA{$$=AllocateTreeProductionNode(fnliteral,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	fnlitdcl{$$=$1;}
	;

xdcl_list:
        %empty
        {
	  $$ = NULL;
	}
|	xdcl_list xdcl LSM{$$=AllocateTreeProductionNode(xdcl_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

vardcl_list:
	vardcl{$$=$1;}
|	vardcl_list LSM vardcl{$$=AllocateTreeProductionNode(vardcl_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

constdcl_list:
	constdcl1{$$=$1;}
|	constdcl_list LSM constdcl1{$$=AllocateTreeProductionNode(constdcl_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

typedcl_list:
	typedcl{$$=$1;}
|	typedcl_list LSM typedcl{$$=AllocateTreeProductionNode(typedcl_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

structdcl_list:
	structdcl{$$=$1;}
|	structdcl_list LSM structdcl{$$=AllocateTreeProductionNode(structdcl_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;


structdcl:
	new_name_list ntype oliteral{$$=AllocateTreeProductionNode(structdcl,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	embed oliteral{$$=AllocateTreeProductionNode(structdcl,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LLPAR embed LRPAR oliteral{$$=AllocateTreeProductionNode(structdcl,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	LSTAR embed oliteral{$$=AllocateTreeProductionNode(structdcl,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	LLPAR LSTAR embed LRPAR oliteral{$$=AllocateTreeProductionNode(structdcl,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LSTAR LLPAR embed LRPAR oliteral{$$=AllocateTreeProductionNode(structdcl,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

	;

packname:
	LIDENTIFIER{$$=$1;}
|	LIDENTIFIER LDOT sym{$$=AllocateTreeProductionNode(packname,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

embed:
	packname{$$=$1;}
	;


arg_type:
	name_or_type{$$=$1;}
|	sym name_or_type{$$=AllocateTreeProductionNode(arg_type,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

arg_type_list:
	arg_type{$$=$1;}
|	arg_type_list LCM arg_type{$$=AllocateTreeProductionNode(arg_type_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

oarg_type_list_ocomma:
        %empty{$$=NULL;}
|	arg_type_list ocomma{$$=AllocateTreeProductionNode(oarg_type_list_ocomma,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

stmt:
        %empty{$$=NULL;}
|	compound_stmt{$$=$1;}
|	common_dcl{$$=$1;}
|	non_dcl_stmt{$$=$1;}
	;

non_dcl_stmt:
	simple_stmt{$$=$1;}
|	for_stmt{$$=$1;}
|	if_stmt{$$=$1;}
|	labelname LCOLON
	stmt{$$=AllocateTreeProductionNode(non_dcl_stmt,3);
				   $$->branch[0] = $1;
                                   $$->branch[1] = $2;		
                                   $$->branch[2] = $3;		}

|	LFALL{$$=$1;}
|	LBREAK onew_name{$$=AllocateTreeProductionNode(non_dcl_stmt,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LCONTINUE onew_name{$$=AllocateTreeProductionNode(non_dcl_stmt,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LGOTO new_name{$$=AllocateTreeProductionNode(non_dcl_stmt,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	LRETURN oexpr_list{$$=AllocateTreeProductionNode(non_dcl_stmt,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

stmt_list:
	stmt{$$=$1;}
|	stmt_list LSM stmt{$$=AllocateTreeProductionNode(stmt_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

new_name_list:
	new_name{$$=$1;}
|	new_name_list LCM new_name{$$=AllocateTreeProductionNode(new_name_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

dcl_name_list:
	dcl_name{$$=$1;}
|	dcl_name_list LCM dcl_name{$$=AllocateTreeProductionNode(dcl_name_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

expr_list:
	expr{$$=$1;}
|	expr_list LCM expr{$$=AllocateTreeProductionNode(expr_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

expr_or_type_list:
	expr_or_type{$$=$1;}
|	expr_or_type_list LCM expr_or_type{$$=AllocateTreeProductionNode(expr_or_type_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

keyval_list:
	keyval{$$=$1;}
|	bare_complitexpr{$$=$1;}
|	keyval_list LCM keyval{$$=AllocateTreeProductionNode(keyval_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	keyval_list LCM bare_complitexpr{$$=AllocateTreeProductionNode(keyval_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

braced_keyval_list:
        %empty{$$=NULL;}
|	keyval_list ocomma{$$=AllocateTreeProductionNode(braced_keyval_list,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

osemi:
        %empty{$$=NULL;}
|	LSM{$$=$1;}
	;

ocomma:
        %empty{$$=NULL;}
|	LCM{$$=$1;}
	;

oexpr:
        %empty{$$=NULL;}
|	expr{$$=$1;}
	;

oexpr_list:
        %empty{$$=NULL;}
|	expr_list{$$=$1;}
	;

osimple_stmt:
        %empty{$$=NULL;}
|	simple_stmt{$$=$1;}
	;

/*
ohidden_funarg_list:
        %empty{$$=NULL;}
|	hidden_funarg_list{$$=$1;}
	;

ohidden_structdcl_list:
        %empty{$$=NULL;}
|	hidden_structdcl_list{$$=$1;}
	;

*/

oliteral:
         %empty{
           $$ = NULL;
         }
|	LLITERAL{$$=$1;}
	;


/*
hidden_import:
	LIMPORT LIDENTIFIER LLITERAL LSM{$$=AllocateTreeProductionNode(hidden_import,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	LVAR hidden_pkg_importsy hidden_type LSM{$$=AllocateTreeProductionNode(hidden_import,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	LCONST hidden_pkg_importsym LASOP hidden_constant LSM{$$=AllocateTreeProductionNode(hidden_import,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LCONST hidden_pkg_importsym hidden_type LASOP hidden_constant LSM{$$=AllocateTreeProductionNode(hidden_import,6);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		
                                   $$->branch[4] = $5;		
                                   $$->branch[5] = $6;		}

|	LTYPE hidden_pkgtype hidden_type LSM{$$=AllocateTreeProductionNode(hidden_import,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	LFUNC hidden_fndcl fnbody LSM{$$=AllocateTreeProductionNode(hidden_import,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

	;

hidden_pkg_importsym:
	hidden_importsym{$$=$1;}
	;


hidden_pkgtype:
	hidden_pkg_importsym{$$=$1;}
	;




hidden_type:
	hidden_type_misc{$$=$1;}
|	hidden_type_func{$$=$1;}
	;




hidden_type_misc:
	hidden_importsym{$$=$1;}
|	LIDENTIFIER{$$=$1;}
|	LLBRK LRBRK hidden_type{$$=AllocateTreeProductionNode(hidden_type_misc,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	LLBRK LLITERAL LRBRK hidden_type{$$=AllocateTreeProductionNode(hidden_type_misc,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	LMAP LLBRK hidden_type LRBRK hidden_type{$$=AllocateTreeProductionNode(hidden_type_misc,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

|	LSTRUCT LLBRA ohidden_structdcl_list LRBRA{$$=AllocateTreeProductionNode(hidden_type_misc,4);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
                                   $$->branch[2] = $3;		
                                   $$->branch[3] = $4;		}

|	LSTAR hidden_type{$$=AllocateTreeProductionNode(hidden_type_misc,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

hidden_type_recv_chan:
	LCOMM LCHAN hidden_type{$$=AllocateTreeProductionNode(hidden_type_misc,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;
*/

/*
hidden_type_func:
	LFUNC LLPAR ohidden_funarg_list LRPAR ohidden_funres{$$=AllocateTreeProductionNode(hidden_type_func,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

	;

hidden_funarg:
	sym hidden_type oliteral{$$=AllocateTreeProductionNode(hidden_funarg,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

hidden_structdcl:
	sym hidden_type oliteral{$$=AllocateTreeProductionNode(hidden_structdcl,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

ohidden_funres:
        %empty{$$=NULL;}
|	hidden_funres{$$=$1;}
	;

hidden_funres:
	LLPAR ohidden_funarg_list LRPAR{$$=AllocateTreeProductionNode(hidden_funres,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

|	hidden_type{$$=$1;}
	;

hidden_literal:
	LLITERAL{$$=$1;}
|	LMINUS LLITERAL{$$=AllocateTreeProductionNode(hidden_literal,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

|	sym{$$=$1;}
	;

hidden_constant:
	hidden_literal{$$=$1;}
|	LLPAR hidden_literal LPLUS hidden_literal LRPAR{$$=AllocateTreeProductionNode(hidden_constant,5);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		
				   $$->branch[3] = $4;		
				   $$->branch[4] = $5;		}

	;
*/
/*
hidden_import_list:
        %empty{$$=NULL;}
|	hidden_import_list hidden_import{$$=AllocateTreeProductionNode(hidden_import_list,2);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;		}

	;

hidden_funarg_list:
	hidden_funarg{$$=$1;}
|	hidden_funarg_list LCM hidden_funarg{$$=AllocateTreeProductionNode(hidden_funarg_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;

hidden_structdcl_list:
	hidden_structdcl{$$=$1;}
|	hidden_structdcl_list LSM hidden_structdcl{$$=AllocateTreeProductionNode(hidden_structdcl_list,3);
				   $$->branch[0] = $1;
				   $$->branch[1] = $2;
				   $$->branch[2] = $3;		}

	;
*/
%%
