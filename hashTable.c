#include "hashTable.h"
#include "LinkedList.h"

struct tokenHashList* searchTable(struct table* scope[Size], char*key)
{
  if(scope[HashFunction(key)])
    {
      /*highly redundant and probably a more efficient way, 
	but it works and shouldn't slow down the code much if at all*/
      struct tokenHashList* tmp = scope[HashFunction(key)]->head;
      if(strcmp(key,tmp->key) == 0)
	return tmp;
      while(tmp->next != NULL)
	{
	  if(strcmp(key,tmp->key) == 0)
	    return tmp;
	  tmp= tmp->next;
	  if(strcmp(key,tmp->key) == 0)
	    return tmp;
	}
      return NULL;
    }
  else
    return NULL;
}

void printTypeHelper(int t)
{
  switch(t)
    {
    case 0:
      fprintf(stderr,"int");
      return;
    case 1:
      fprintf(stderr,"float");
      return;
    case 2:
      fprintf(stderr,"string");
      return;
    case 3:
      fprintf(stderr,"bool");
      return;
    case 4:
      fprintf(stderr,"other/undefined");
      return;
      break;
    default:
      {
	struct ScopeList* CurrScope = ScopeListHead;
	int indexInList = 4;
	while(CurrScope)
	  {
	    if(CurrScope->structType)
	      {
		indexInList++;
		if(indexInList == t)
		  {
		    fprintf(stderr,"%s",CurrScope->scopeName);
		  }
	      }
	    CurrScope = CurrScope->next;
	  }
	return;
      }
    }
}


void printType(int t,int aFlag,int m)
{
  if((aFlag & 0x8) == 0x8)
    {
      fprintf(stderr,"function\n");
      return;
    }
  if((aFlag & 0x2) == 0x2)
    {
      fprintf(stderr,"map[");
      printTypeHelper(m);
      fprintf(stderr,"]");
    }
  if((aFlag & 0x4) == 0x4)
    {
      fprintf(stderr,"*");
    }
  if((aFlag & 0x40) == 0x40)
    {
      fprintf(stderr,"[]");
    }
  printTypeHelper(t);
  if((aFlag & 0x10) == 0x10)
    {
      fprintf(stderr," (Parameter)");
    }
  fprintf(stderr,"\n");		  
}


void printHashtable(struct ScopeList* scope)
{
  int i;
  struct table **tableVal;
  tableVal = scope->tableVal;
  struct tokenHashList* singleList;
  fprintf(stderr,"--- symbol table for: %s ---\n",scope->scopeName);
  for(i = 0; i < Size; i++)
    {
      if(tableVal[i])
	{
	  singleList = tableVal[i]->head;
	  while(singleList)
	    {
	      fprintf(stderr,"    ");
	      fprintf(stderr,"%s ",singleList->key);
	      printType(singleList->datatype,singleList->auxFlags,singleList->mapType);
	      singleList = singleList->next;
	    }
	}
    }
  fprintf(stderr,"---\n");
}


struct tokenHashList* addToTable(struct ScopeList* scope, struct token* value, int type, int flags, char* filename, int mapType)
{
  /*checks to see if collision exists*/
  if(!(scope->tableVal[HashFunction(value->text)]))
    {
      scope->tableVal[HashFunction(value->text)]             = (struct table*)malloc(sizeof(struct table));
      scope->tableVal[HashFunction(value->text)]->head       = (struct tokenHashList*)malloc(sizeof(struct tokenHashList));
      scope->tableVal[HashFunction(value->text)]->head->key  = strdup(value->text);
      scope->tableVal[HashFunction(value->text)]->head->datatype  = type;
      scope->tableVal[HashFunction(value->text)]->head->auxFlags  = flags;
      scope->tableVal[HashFunction(value->text)]->head->mapType  = mapType;
      scope->tableVal[HashFunction(value->text)]->head->next      = NULL;
      return scope->tableVal[HashFunction(value->text)]->head;
    }
  else
    {
      /*if collision does exist then go down the list till NULL then add bucket */
      struct tokenHashList* tmp = scope->tableVal[HashFunction(value->text)]->head;
      if(strcmp(value->text,tmp->key) == 0)
	{
	  fprintf(stderr,"Error redeclaration of Variable: \"%s\" On file: %s Error on line: %d\n",value->text,filename,value->lineno);
	  if(strcmp(scope->scopeName,"Global Scope") == 0)
	    {
	      fprintf(stderr,"Note: Redeclaration was at the Global Scope\n");
	    }
	  exit(3);
	}
      while(tmp->next != NULL)
	{
	  if(strcmp(value->text,tmp->key) == 0)
	    {
	      fprintf(stderr,"Error redeclaration of : \"%s\" On file: %s Error on line: %d\n",value->text,filename,value->lineno);
	      if(strcmp(scope->scopeName,"Global Scope") == 0)
		{
		  fprintf(stderr,"Note: Redeclaration was at the Global Scope\n");
		}
	      exit(3);
	    }
	  tmp=tmp->next;
	}
      tmp->next = (struct tokenHashList*)malloc(sizeof(struct tokenHashList));
      tmp       = tmp->next;
      tmp->key  = strdup(value->text);
      tmp->datatype= type;
      tmp->auxFlags= flags;
      tmp->mapType= mapType;
      tmp->next    = NULL;
      return tmp;
    }
  return NULL;
}

struct tokenHashList* addToGlobal(struct ScopeList* scope, char* text, int type, int flags, char* filename)
{
  struct token* newToken = (struct token*)malloc(sizeof(struct token));
  newToken->category = 0;
  newToken->text = text;
  newToken->lineno = 0;
  newToken->ival = 0;
  newToken->dval = 0;
  newToken->sval = NULL;
  struct tokenHashList* temp = addToTable(scope,newToken,type,flags,filename,0);
  free(newToken);
  return temp;
}

int removeFromTable(struct table* scope[Size], struct token* key)
{
  /*typical remove from the middle of a LinkedList function*/
  struct tokenHashList* tmp = scope[HashFunction(key->text)]->head;
  struct tokenHashList* tmp2 = tmp;
  if(tmp)
    {
      if(strcmp(tmp->key, key->text) == 0)
	{
	  scope[HashFunction(key->text)]->head = tmp->next;
	  free(tmp->key);
	  free(tmp);
	}
      else
	{
	  tmp = tmp->next;
	  if(!tmp)
	    {
	      return -1;
	    }
	  while(strcmp(tmp->key, key->text) != 0)
	    {
	      tmp2= tmp;
	      tmp = tmp->next;
	      if(!tmp)
		{
		  return -1;
		}
	    }
	  //KEY FOUND
	  tmp2->next = tmp->next;
	  free(tmp->key);
	  free(tmp);
	}
    }
  else
    {
      return -1;
    }
  return 1;
}


int HashFunction(char* key)
{
  int hashval=0;
  unsigned int i;
  for(i = 0; i < strlen(key); i++)
    {
      hashval += key[i]*i;
    }
  hashval *= 3593;
  hashval %= Size;
  return hashval;
}


struct ParameterList* addParameterList(struct tokenHashList* value, struct ScopeList* scope)
{
  if(!scope->head)
    {
      scope->head = (struct ParameterList*)malloc(sizeof(struct ParameterList));
      scope->head->value = value;
      scope->head->next = NULL;
      return scope->head;
    }
  else
    {
      struct ParameterList* tmp = scope->head;
      while(tmp->next != NULL)
	{
	  tmp = tmp->next;
	}
      tmp->next = (struct ParameterList*)malloc(sizeof(struct ParameterList));
      tmp = tmp->next;
      tmp->next = NULL;
      tmp->value = value;
      return tmp;
    }
  return NULL;
}

void printParameters(struct ScopeList* tmp)
{
  struct ParameterList* temp = tmp->head;
  while(temp)
    {
      fprintf(stderr,"%s\t",temp->value->key);
      temp= temp->next;
    }
  fprintf(stderr,"\n");
}
