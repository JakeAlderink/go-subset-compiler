#include "tree.h"
#include <math.h>

extern struct tokenTree* head;
extern char* Lfilename;
extern char lastid[256];
extern int lineNumber;


void fixsval(char* sval);
int printTreeHelper(struct tokenTree* t, int depth);  /* prints out the formated output for the linked list for homework 1 */
  
int addToTree(int category)
{
  if(head)
    {
      head->value->category = category;
    }
  return 1;
}



void fixsval(char sval[])
{
  /*
    fixes strings to their proper form
    such as correcting newlines and tabs and other escape characters
  */
  char temp[strlen(sval)];

  /*
    index refering to the orginal string index
  */
  unsigned int oindex = 1;

  /*
    index refereing to the new string index for after all info has been processed
  */
  int nindex = 0;
  
  for(oindex = 1; oindex < strlen(sval)-1; oindex++)
    {
      if(sval[oindex] == '\\')
	{
	  switch(sval[oindex+1])
	    {
	    case '\?':
	      temp[nindex]='\?';
	      break;
	    case '\'':
	      temp[nindex] = '\'';
	      break;
	    case '\"':
	      temp[nindex] = '\"';
	      break;
	    case '\\':
	      temp[nindex] = '\\';
	      break;
	    case 'a':
	      temp[nindex] = '\a';
	      break;
	    case 'b':
	      temp[nindex] = '\b';
	      break;
	    case 'f':
	      temp[nindex] = '\f';
	      break;
	    case 'n':
	      temp[nindex] = '\n';
	      break;
	    case 'r':
	      temp[nindex] = '\r';
	      break;
	    case 't':
	      temp[nindex] = '\t';
	      break;
	    case 'v':
	      temp[nindex] = '\v';
	      break;
	    case '0':
	      temp[nindex] = '\0';
	      break;
	      
	    }
	  nindex++;
	  oindex++;
	}
      else
	{
	  temp[nindex] = sval[oindex];
	  nindex++;
	}
    }
  temp[nindex] = '\0';
  strcpy(sval,temp);
  
}

int printTree(struct tokenTree* head)
{
  return printTreeHelper(head,0);
}


int printTreeHelper(struct tokenTree* t, int depth)  /* prints out the formated output for the linked list for homework 1 */
{
  int i;
  if(t->nBranches == 0)
    printf("%*s %d: %s\n", depth*2, " ", t->value->category, t->value->text);
  else
    {
      printf("%*s", depth*2, " ");
      printIntToStringProdRule(t->prodrule);
      printf(": %d\n",t->nBranches);
    }
  for(i=0; i<9; i++)
    {
      if(t->branch[i])
	printTreeHelper(t->branch[i], depth+1);
    }
  return -1;  
}


struct tokenTree* AllocateTreeLeafNode(int category)
{
  struct tokenTree* tmp = (struct tokenTree*)malloc(sizeof(struct tokenTree));
  tmp->code = NULL;
  tmp->place = NULL;
  tmp->first = NULL;
  tmp->follow = NULL;
  tmp->True = NULL;
  tmp->False = NULL;
  tmp->dataType =NULL;
  tmp->placeVal = -1;
  tmp->nBranches = 0;
  tmp->prodrule = -1;
  tmp->filename = strdup(Lfilename);
  unsigned int i;
  for( i = 0; i < 9; i++)
    {
      tmp->branch[i] = NULL;
    }
  tmp->value = (struct token*)malloc(sizeof(struct token));
  struct token* tokenval = tmp->value;
  tokenval->category = category;
  tokenval->text=strdup(lastid);
  tokenval->lineno = lineNumber;
  tokenval->sval = NULL;
  tokenval->ival = -10000;
  tokenval->dval = NAN;
  if(category == LLITERAL)
    {
      if(lastid[0] == '\"')
	{/*string*/
	  tokenval->sval = strdup(lastid); 
	  fixsval(tokenval->sval);  
	}
      else if(lastid[0] =='\'')
	{/*rune*/
	  tokenval->sval = strdup(lastid); 
	}
      else if(strcmp(lastid,"true") == 0 || strcmp(lastid,"false") == 0)
	{
	  /*bool*/
	}
      else if(!strchr(lastid,'.'))
	{/*int*/
	  tokenval->ival = atoi(lastid); 
	}
      else
	{/*float*/
	   tokenval->dval = atof(lastid); 
	}
    }  
  return tmp;
}


struct tokenTree* AllocateTreeProductionNode(int nprodrule,int nkids)
{
  if(debug)
    {
      printf("production rule: " );
      printIntToStringProdRule(nprodrule);
      printf("\n");
    }
  struct tokenTree* tmp = (struct tokenTree*)malloc(sizeof(struct tokenTree));
  tmp->code = NULL;
  tmp->place = NULL;
  tmp->first = NULL;
  tmp->follow = NULL;
  tmp->True = NULL;
  tmp->False = NULL;
  tmp->dataType = NULL;
  tmp->placeVal = -1;
  tmp->nBranches = nkids;
  tmp->prodrule = nprodrule;
  tmp->filename = strdup(Lfilename);
  int i;
  for( i = nkids; i < 9; i++)
    {
      tmp->branch[i] = NULL;
    }
  tmp->value = NULL;
  return tmp;
}
