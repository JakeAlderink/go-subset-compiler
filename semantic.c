#include "semantic.h"
#include <math.h>

struct tokenHashList* copyVal;
struct tokenHashList* baseTypes[4];

void arrayValueRec(struct tokenTree* placement, struct ScopeList* CurrScope);
int returnTypeBasedOnReservedword(struct token* value);

int areSameType(struct tokenHashList* x,struct tokenHashList* y, int xDereference, int yDereference);


void buildSymbolTable(struct tokenTree* treeHead, struct ScopeList* CurrScope);
void depthFirst(struct tokenTree* placement, struct ScopeList* CurrScope);

struct ScopeList* ScopeFromTypeNumber(int type,struct tokenTree*);
struct tokenHashList* checkUnaryExpression(struct tokenTree* placement, struct tokenHashList* lVal,struct tokenHashList* rVal);
struct tokenHashList* getTypeFromLiteral(struct tokenTree* placement, struct ScopeList* CurrScope);
struct tokenHashList* expressionTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope);
struct tokenHashList* nondclTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope);
struct tokenHashList* simplestmtTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope);
void pseudocalTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope,int depth,int LLSize,struct ScopeList* funcScope);
struct tokenHashList* caseOfPExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope);
void caseOfUnaryExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope);
void caseOfSimpleExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope);
void caseOfPCallExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope);
void caseOfNormalExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope);
struct tokenHashList* checkPExpression(struct tokenTree* placement, struct ScopeList* CurrScope);
struct tokenHashList* expressionType(struct tokenTree* placement, struct ScopeList* CurrScope);
  
void addParameterValues(struct tokenTree* placement, struct ScopeList* CurrScope);
void addCommonDeclarations(struct tokenTree* placement, struct ScopeList* CurrScope);


void newTypeDeclaration(struct tokenTree* placement, struct ScopeList* CurrScope);
void newNameListHandler(struct tokenTree* placement,struct tokenTree* type,struct ScopeList* CurrScope);
void structArgAdder(struct tokenTree* placement, struct ScopeList* CurrScope);
void dclNameListHandler(struct tokenTree* placement,int type,int auxflag,int mapType, struct ScopeList* CurrScope);




void AddFmtToGlobal(struct ScopeList* scope, struct tokenTree* value);
void AddRandToGlobal(struct ScopeList* scope, struct tokenTree* value);
void AddTimeToGlobal(struct ScopeList* scope, struct tokenTree* value);

void checkType(struct tokenTree* placement, struct ScopeList* CurrScope);
//int SizeOf(struct ParameterList* LL);

void BaseTypesInit();


void semantic()
{
  copyVal = NULL;
  /* Main function for semantic Analysis */
  BaseTypesInit();
  struct ScopeList* currScope;
  currScope = ScopeInit();                   /*LinkedList.c*/
  struct TreeList* tmp = TreeListHead;
  while(tmp->next != NULL)          /* Traverse List of trees and builds a new scope for all functions in all files*/
    {
      buildSymbolTable(tmp->treeHead, currScope);
      tmp = tmp->next;
    }
  buildSymbolTable(tmp->treeHead, currScope); /*This just makes sure the element of the list is also anaylzed*/
  tmp = TreeListHead;
  if(typeCheckIsOn)
    {
      while(tmp->next != NULL)          /* Traverse List of trees and builds a new scope for all functions in all files*/
	{
	  checkType(tmp->treeHead, currScope);
	  tmp = tmp->next;
	}
      checkType(tmp->treeHead, currScope); /*This just makes sure the element of the list is also anaylzed*/
    }
  /*This last pass is completely extra and wasteful but since this is a class and not industry 
    i will have this pass purely be dedicated to finding array declarations and setting their value to the correct value*/
  arrayValueRec(tmp->treeHead,currScope);
}


void arrayValueRec(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  if(!placement)
    return;
  if(placement->prodrule == xfndcl)
    {
      CurrScope = GetScope(placement->branch[1]->branch[0]->value->text);
    }
  if(placement->prodrule == typedcl)
    {
      CurrScope = GetScope(placement->branch[0]->value->text);
    }
  else if(placement->prodrule == vardcl || placement->prodrule == arg_type || placement->prodrule == structdcl)
    {
      if(placement->branch[1]->prodrule != othertype)
	{
	  return;
	}
      if(placement->branch[1]->branch[0]->value->category == LLBRK)
	{	  
	  searchTable(CurrScope->tableVal,placement->branch[0]->value->text)->arraySize = placement->branch[1]->branch[1]->value->ival;
	}

    }
  int i = 0;
  int endingi = 9;
  for(; i < endingi; i++)
    {
      arrayValueRec(placement->branch[i],CurrScope);
    }
  return;  
}

		   
void BaseTypesInit()
{
  /*int*/
  baseTypes[0] = (struct tokenHashList*) malloc(sizeof(struct tokenHashList));
  baseTypes[0]->datatype = 0;
  baseTypes[0]->next = NULL;
  baseTypes[0]->key = NULL;
  baseTypes[0]->auxFlags = 0x0;
  baseTypes[0]->mapType = 4;
  /*float64*/
  baseTypes[1] = (struct tokenHashList*) malloc(sizeof(struct tokenHashList));
  baseTypes[1]->datatype = 1;
  baseTypes[1]->next = NULL;
  baseTypes[1]->key = NULL;
  baseTypes[1]->auxFlags = 0x0;
  baseTypes[1]->mapType = 4;
  /*string*/
  baseTypes[2] = (struct tokenHashList*) malloc(sizeof(struct tokenHashList));
  baseTypes[2]->datatype = 2;
  baseTypes[2]->next = NULL;
  baseTypes[2]->key = NULL;
  baseTypes[2]->auxFlags = 0x0;
  baseTypes[2]->mapType = 4;
  /*bool*/
  baseTypes[3] = (struct tokenHashList*) malloc(sizeof(struct tokenHashList));
  baseTypes[3]->datatype = 3;
  baseTypes[3]->next = NULL;
  baseTypes[3]->key = NULL;
  baseTypes[3]->auxFlags = 0x0;
  baseTypes[3]->mapType = 4;
}

void PrintType(struct tokenHashList* value)
{
  if((value->auxFlags & 0x8) == 0x8)
    {
      fprintf(stderr,"(function return)");
    }
  if((value->auxFlags & 0x2) == 0x2)
    {
      fprintf(stderr,"map[");
      printTypeHelper(value->mapType);
      fprintf(stderr,"]");
    }
  if((value->auxFlags & 0x4) == 0x4)
    {
      fprintf(stderr,"*");
    }
  if((value->auxFlags & 0x40) == 0x40)
    {
      fprintf(stderr,"[]");
    }
  printTypeHelper(value->datatype);
  /* if((value->auxFlags & 0x10) == 0x10) */
  /*   { */
  /*     printf(" (Parameter)"); */
  /*   } */
}


struct tokenHashList* GetListI(struct ScopeList* CurrScope,int depth)
{
  int i = 0;
  struct ParameterList* tmp = CurrScope->head;
  for(; i < depth; i++)
    {
      tmp = tmp->next;
    }
  return tmp->value;
}


struct tokenHashList* expressionTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope)
{
  struct tokenHashList* lVal = NULL;
  struct tokenHashList* rVal = NULL;
  if(placement->branch[0]->prodrule == expr)
    {
      lVal = expressionTypeCheck(placement->branch[0],CurrScope);
    }
  else if(placement->branch[0]->prodrule == pexpr)
    {
      lVal = expressionTypeCheck(placement->branch[0]->branch[1],CurrScope);
    }
  else if (placement->branch[0]->prodrule == pseudocall)
    {
      /*check arguments in fucntion are correct*/
      pseudocalTypeCheck(placement->branch[0]->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[0]->branch[0]->value->text)->head),GetScope(placement->branch[0]->branch[0]->value->text));
      /*check return type of function*/
      lVal = typeOf(placement->branch[0]->branch[0],CurrScope);      
    }
  else if (placement->branch[0]->prodrule == pexpr_no_paren)
    {
      lVal = checkPExpression(placement->branch[0], CurrScope); 
    }
  else if(placement->branch[0]->prodrule == -1)
    {
      if(placement->branch[0]->value->category == LIDENTIFIER || placement->branch[0]->value->category == LLITERAL)
	{
	  lVal = getTypeFromLiteral(placement->branch[0],CurrScope);
	  if((lVal->auxFlags&0x8) == 0x8)
	    {
	      fprintf(stderr, "Error: Function refered to as literal line:%d file:%s\n",placement->branch[0]->value->lineno,placement->branch[0]->filename);
	    }
	}
      else
	{
	  printf("Houston We have a problem\n");
	  exit(3);
	}      
    }
  if(placement->branch[2]->prodrule == expr)
    {
      rVal = expressionTypeCheck(placement->branch[2],CurrScope);
    }
  else if(placement->branch[2]->prodrule == pexpr)
    {
      rVal = expressionTypeCheck(placement->branch[2]->branch[1],CurrScope);
    }
  else if (placement->branch[2]->prodrule == pseudocall)
    {
      pseudocalTypeCheck(placement->branch[2]->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[2]->branch[0]->value->text)->head),GetScope(placement->branch[2]->branch[0]->value->text));
      rVal = typeOf(placement->branch[2]->branch[0],CurrScope);
    }
  else if (placement->branch[2]->prodrule == pexpr_no_paren)
    {
      rVal = checkPExpression(placement->branch[2], CurrScope); 
    }

  else if(placement->branch[2]->prodrule == -1)
    {
      if(placement->branch[2]->value->category == LIDENTIFIER || placement->branch[2]->value->category == LLITERAL)
	{
	  rVal = getTypeFromLiteral(placement->branch[2],CurrScope);
	  if((rVal->auxFlags&0x8) == 0x8)
	    {
	      fprintf(stderr, "Error: Function refered to as literal line:%d file:%s\n",placement->branch[0]->value->lineno,placement->branch[0]->filename);
	    }
	}
      else
	{
	  printf("Houston We have a problem\n");
	  printf("line:%d text:%s\n",placement->branch[2]->value->lineno,placement->branch[2]->value->text);
	  exit(3);
	}
    }
  return checkUnaryExpression(placement,lVal,rVal);
}


struct tokenHashList* nondclTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope)
{
  struct tokenHashList* rVal = NULL;
  if(placement->branch[0]->value->category == LRETURN)
    {
      if(debug)
	{
	  fprintf(stderr,"In fucntion scope:%s, at line:%d\n",CurrScope->scopeName,placement->branch[0]->value->lineno);
	  fflush(stderr);
	}
      /* return with no return expression*/
      if(!placement->branch[1])
	{
	  /*function is void*/
	  if(searchTable(ScopeListHead->tableVal,CurrScope->scopeName)->datatype == 4)
	    {
	      /*good*/
	    }
	  else
	    {
	      fprintf(stderr,"ERROR: line:%d file:%s function return expression expected\n",placement->branch[0]->value->lineno,placement->filename);
	      exit(3);
	    }
	  return NULL;
	}
      if(placement->branch[1]->prodrule == -1)
	{
	  if(placement->branch[1]->value->category == LIDENTIFIER || placement->branch[1]->value->category == LLITERAL)
	    {
	      rVal = getTypeFromLiteral(placement->branch[1],CurrScope);
	    }
	}
      else if(placement->branch[1]->prodrule == pexpr)
	{
	  rVal = expressionTypeCheck(placement->branch[1]->branch[1],CurrScope);
	}
      else if(placement->branch[1]->prodrule == expr)
	{
	  rVal = expressionTypeCheck(placement->branch[1],CurrScope);
	}
      else if(placement->branch[1]->prodrule == pseudocall)
	{
	  pseudocalTypeCheck(placement->branch[1]->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[1]->branch[0]->value->text)->head),GetScope(placement->branch[1]->branch[0]->value->text));
	  rVal = typeOf(placement->branch[1]->branch[0],CurrScope);
	}
      else
	{
	  printf("new case not supproted in vgo\n");
	  exit(3);
	}
      if(!areSameType(rVal,searchTable(ScopeListHead->tableVal,CurrScope->scopeName),0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s function return expected type:",placement->branch[1]->value->lineno,placement->filename);
	  PrintType(searchTable(ScopeListHead->tableVal,CurrScope->scopeName));
	  fprintf(stderr,"  while the return statement provide type:");
	  PrintType(rVal);
	  fprintf(stderr,"\n");
	  exit(3);
	}
    }
  return NULL;
}


struct tokenHashList* simplestmtTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope)
{
  struct tokenHashList* lVal = NULL;
  struct tokenHashList* rVal = NULL;
  if(placement->branch[0]->prodrule == expr)
    {
      fprintf(stderr,"ERROR: line:%d file:%s expression on left hand side was of assignment not allowed\n",placement->branch[1]->value->lineno,placement->filename);
      exit(3);
    }
  else if(placement->branch[0]->prodrule == pexpr)
    {
      fprintf(stderr,"ERROR: line:%d file:%s expression on left hand side was of assignment not allowed\n",placement->branch[1]->value->lineno,placement->filename);
      exit(3);
    }
  else if (placement->branch[0]->prodrule == pseudocall)
    {
      fprintf(stderr,"ERROR: line:%d file:%s function on left hand side was of assignment not allowed\n",placement->branch[1]->value->lineno,placement->filename);
      exit(3);
    }
  else if(placement->branch[0]->prodrule == -1)
    {
      if(placement->branch[0]->value->category == LIDENTIFIER )
	{
	  lVal = getTypeFromLiteral(placement->branch[0],CurrScope);
	  if((lVal->auxFlags & 0x1) == 0x1)
	    {
	      fprintf(stderr,"ERROR: line:%d file:%s Const value on left hand side of assignment not allowed\n",placement->branch[1]->value->lineno,placement->filename);
	      exit(3);
	    }
	  if(!lVal)
	    {
	      printf("NULL\n");
	      exit(3);
	    }
	}
      else if( placement->branch[0]->value->category == LLITERAL)
	{
	  fprintf(stderr,"ERROR: line:%d file:%s Literal value on left hand side was of assignment not allowed\n",placement->branch[1]->value->lineno,placement->filename);
	  exit(3);
	}
      else
	{
	  printf("Houston We have a problem\n");
	  exit(3);
	}      
    }
  else if( placement->branch[0]->prodrule == pexpr_no_paren)
    {
      lVal = checkPExpression(placement->branch[0], CurrScope);
      if(!lVal)
	{
	  printf("NULL\n");
	  exit(3);
	}
    }
  else
    {
      printf("prodrule:%d not supported? line:%d:\n",placement->branch[0]->prodrule,placement->branch[1]->value->lineno);
      exit(3);
    }
  if(placement->branch[1]->value->category == LINC ||placement->branch[1]->value->category == LDEC)
    {
      if(!areSameType(lVal,baseTypes[0],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s Literal value on left hand side was not of type int not allowed for '++' and '--'\n",placement->branch[1]->value->lineno,placement->filename);
	  exit(3);
	}
      return lVal;
    }
  if(placement->branch[2]->prodrule == expr)
    {
      rVal = expressionTypeCheck(placement->branch[2],CurrScope);
    }
  else if(placement->branch[2]->prodrule == pexpr)
    {
      rVal = expressionTypeCheck(placement->branch[2]->branch[1],CurrScope);
    }
  else if (placement->branch[2]->prodrule == pseudocall)
    {
      pseudocalTypeCheck(placement->branch[2]->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[2]->branch[0]->value->text)->head),GetScope(placement->branch[2]->branch[0]->value->text));
      rVal = typeOf(placement->branch[2]->branch[0],CurrScope);
    }
  else if(placement->branch[2]->prodrule == -1)
    {
      if(placement->branch[2]->value->category == LIDENTIFIER || placement->branch[2]->value->category == LLITERAL)
	{
	  rVal = getTypeFromLiteral(placement->branch[2],CurrScope);
	}
      else
	{
	  printf("Houston We have a problem\n");
	  printf("line:%d text:%s\n",placement->branch[2]->value->lineno,placement->branch[2]->value->text);
	  exit(3);
	}
    }
  return checkUnaryExpression(placement,lVal,rVal);

}

struct tokenHashList* checkUnaryExpression(struct tokenTree* placement, struct tokenHashList* lVal,struct tokenHashList* rVal)
{
  /*check left*/
  /*check right*/
  if(!areSameType(lVal,rVal,0,0))
    {
      fprintf(stderr,"ERROR: line:%d file:%s expression left hand side was of type:",placement->branch[1]->value->lineno,placement->filename);
      PrintType(lVal);
      fprintf(stderr,"  while the right hand side was:");
      PrintType(rVal);
      fprintf(stderr,"\n");
      exit(3);
    }  
  switch(placement->branch[1]->value->category)
    {
    case LASOP:
      return NULL;
      break;
    case LPLUSEQ:
      if(areSameType(lVal,baseTypes[3],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s expression \'%s\' does not support values of this type\n",placement->value->lineno,placement->filename,placement->branch[2]->value->text);
	  exit(3);
	}
      return NULL;
      break;
    case LMINUSEQ:
      if(areSameType(lVal,baseTypes[3],0,0) || areSameType(lVal,baseTypes[2],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s expression \'%s\' does not support values of this type\n",placement->value->lineno,placement->filename,placement->branch[2]->value->text);
	  exit(3);
	}
      return NULL;
      break;
    case LEQ:
      placement->dataType = baseTypes[3];
      return baseTypes[3];
      break;
    case LLT:
    case LGT:
      if(areSameType(lVal,baseTypes[3],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s expression \'%s\' does not support values of this type\n",placement->value->lineno,placement->filename,placement->branch[2]->value->text);
	  exit(3);
	}
      placement->dataType = baseTypes[3];
      return baseTypes[3];
      break;
    case LPLUS:
      if(areSameType(lVal,baseTypes[3],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s expression \'%s\' does not support values of this type\n",placement->value->lineno,placement->filename,placement->branch[2]->value->text);
	  exit(3);
	}
      break;
    case LMINUS:
    case LSTAR:
    case LDIVIDE:
      if(areSameType(lVal,baseTypes[3],0,0) || areSameType(lVal,baseTypes[2],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s expression \'%s\' does not support values of this type\n",placement->value->lineno,placement->filename,placement->branch[2]->value->text);
	  exit(3);
	}
      break;
    case LANDAND:
    case LOROR:
      if(!areSameType(lVal,baseTypes[3],0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s expression \'%s\' does not support values of this type\n",placement->value->lineno,placement->filename,placement->branch[2]->value->text);
	  exit(3);
	}      
      break;
    }
  placement->dataType = lVal;
  return lVal;
}

struct tokenHashList* getTypeFromLiteral(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  struct tokenHashList* tmp;
  if(placement->value->category == LIDENTIFIER)
    {
      tmp = typeOf(placement,CurrScope);
    }
  else if(placement->value->category ==LLITERAL)
    {
      if(placement->value->ival != -10000)
	{
	  tmp = baseTypes[0];
	}
      else if(placement->value->sval != NULL)
	{
	  tmp = baseTypes[2];
	}
      else if(placement->value->dval != NAN)
	{
	  tmp = baseTypes[1];
	}
      else
	{
	  tmp = baseTypes[3];
	}
    }
  return tmp;
}


void pseudocalTypeCheck(struct tokenTree* placement,struct ScopeList* CurrScope,int depth, int LLSize, struct ScopeList* funcScope)
{
  /*case of 0 arguments*/
  if(placement->value && placement->value->category == LRPAR)
    {
      if(debug)
	{
	  printf("found 0 arguments\n");
	}
      return;
    }
  /*case of 1 argumnet*/
  if(placement->prodrule == -1)
    {
      caseOfSimpleExpression(placement,CurrScope,0,funcScope);
      return;
    }
  else if(placement->prodrule == uexpr)
    {
      caseOfUnaryExpression(placement,CurrScope,0,funcScope);
      return;
    }
  else if(placement->prodrule == pexpr_no_paren)
    {
      checkPExpression(placement,CurrScope);
      return;
    }
  else if(placement->prodrule == pseudocall)
    {
      caseOfPCallExpression(placement,CurrScope,0,funcScope);
      return;
    }
  else if(placement->prodrule == expr)
    {
      caseOfNormalExpression(placement,CurrScope,0,funcScope);
      return;
    }
  if(placement->branch[0]->prodrule != expr_or_type_list)
    {
      /*TODO check for expressions as arguments
	expr(z+1) or pexpr_no_paren(z.x)
      */
      if(placement->branch[0]->prodrule == -1)
	{
	  caseOfSimpleExpression(placement->branch[0],CurrScope,0,funcScope);
	}
      else if(placement->branch[0]->prodrule == uexpr)
	{
	  caseOfUnaryExpression(placement->branch[0],CurrScope,0,funcScope);
	}
      else if(placement->branch[0]->prodrule == pexpr_no_paren)
	{
	  checkPExpression(placement->branch[0],CurrScope);
	}
      else if(placement->branch[0]->prodrule == pseudocall)
	{
	  caseOfPCallExpression(placement->branch[0],CurrScope,0,funcScope);
	}
      else if(placement->prodrule == expr)
	{
	  caseOfNormalExpression(placement->branch[0],CurrScope,0,funcScope);
	}
    }
  else if(placement->branch[0]->prodrule == expr_or_type_list)
    {
      pseudocalTypeCheck(placement->branch[0],CurrScope,depth+1,LLSize,funcScope);
    }
  if(placement->branch[2]->prodrule == -1)
    {
      caseOfSimpleExpression(placement->branch[2],CurrScope,LLSize-depth-1,funcScope);
    }
  else if(placement->branch[2]->prodrule == uexpr)
    {
      caseOfUnaryExpression(placement->branch[2],CurrScope,LLSize-depth-1,funcScope);
    }
  else if(placement->branch[2]->prodrule == pexpr_no_paren)
    {
      checkPExpression(placement->branch[2],CurrScope);
    }
  else if(placement->branch[2]->prodrule == pseudocall)
    {
      caseOfPCallExpression(placement->branch[2],CurrScope,LLSize-depth-1,funcScope);
    }
  else if(placement->branch[2]->prodrule == expr)
    {
      caseOfNormalExpression(placement->branch[2],CurrScope,LLSize-depth-1,funcScope);
    }
}


void caseOfNormalExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope)
{
  struct tokenHashList* tmp = expressionTypeCheck(placement,CurrScope);
  if(!areSameType(tmp,GetListI(funcScope,depth),0,0))
    {
      fprintf(stderr,"ERROR: line:%d file:%s argument %d was of type:",placement->branch[0]->value->lineno,placement->filename,depth);
      PrintType(tmp);
      fprintf(stderr,"  function parameter expected:");
      PrintType(GetListI(funcScope,depth));
      fprintf(stderr,"\n");
      exit(3);
    }
  return;
}



void caseOfPCallExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope)
{
  pseudocalTypeCheck(placement->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[0]->value->text)->head),GetScope(placement->branch[0]->value->text));
  if(!areSameType(typeOf(placement->branch[0],CurrScope),GetListI(funcScope,depth),0,0))
    {
      fprintf(stderr,"ERROR: line:%d file:%s argument %d was of type:",placement->branch[0]->value->lineno,placement->filename,depth);
      PrintType(typeOf(placement->branch[0],CurrScope));
      fprintf(stderr,"  function parameter expected:");
      PrintType(GetListI(funcScope,depth));
      fprintf(stderr,"\n");
      exit(3);
    }
}

struct tokenHashList* caseOfPExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope)
{
  /*
    func(z[2])
  */
  if(placement->branch[1]->value->category == LLBRK)
    {
      if(debug)
	{
	  fprintf(stderr,"searching for:%s in scope:%s\n",placement->branch[0]->value->text,CurrScope->scopeName);
	  fflush(stdout);
	}
      if(!areSameType(typeOf(placement->branch[0],CurrScope),GetListI(funcScope,depth),1,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s argument %d was of type:",placement->branch[0]->value->lineno,placement->filename,depth);
	  PrintType(typeOf(placement->branch[0],CurrScope));
	  fprintf(stderr,"  function parameter expected:");
	  PrintType(GetListI(funcScope,depth));
	  fprintf(stderr,"\n");
	  exit(3);
	}
    }
  /*
    func(z.x)
  */
  else if(placement->branch[1]->value->category == LDOT)
    {
      struct ScopeList* structScope = GetScope(placement->branch[0]->value->text);
      if(!areSameType(searchTable(structScope->tableVal,placement->branch[2]->value->text),GetListI(funcScope,depth),0,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s argument %d was of type:",placement->branch[0]->value->lineno,placement->filename,depth);
	  PrintType(searchTable(structScope->tableVal,placement->branch[2]->value->text));
	  fprintf(stderr,"  function parameter expected:");
	  PrintType(GetListI(funcScope,depth));
	  fprintf(stderr,"\n");
	  exit(3);
	}
    }
  return NULL;
}


struct tokenHashList* checkPExpression(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  struct tokenHashList* lval = NULL;
  
  if(placement->branch[0]->prodrule == pexpr_no_paren)
    {
      lval = checkPExpression(placement->branch[0],CurrScope);
    }
  else if(placement->branch[0]->prodrule == pseudocall)
    {
      pseudocalTypeCheck(placement->branch[0]->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[0]->branch[0]->value->text)->head),GetScope(placement->branch[0]->branch[0]->value->text));
      /*check return type of function*/
      lval = typeOf(placement->branch[0],CurrScope);      
    }
  else
    {
      lval = typeOf(placement->branch[0],CurrScope);   /*getType(placement->branch[0]->prodrule)*/
      if((lval->auxFlags&0x8) == 0x8)
	{
	  fprintf(stderr, "Error: Function refered to as literal line:%d file:%s\n",placement->branch[0]->value->lineno,placement->branch[0]->filename);
	}
    }
  
  if(placement->branch[1]->prodrule == -1)
    {
      if(placement->branch[1]->value->category == LDOT)
	{
	  struct tokenHashList* tmp = searchTable(ScopeFromTypeNumber(lval->datatype,placement->branch[1])->tableVal,placement->branch[2]->value->text);
	  if(tmp)
	    {
	      return tmp;
	    }
	  else
	    {
	      /*TODO*/
	      fprintf(stderr,"no such member of the struct\n");
	      exit(3);
	    }
	}
      else if(placement->branch[1]->value->category == LLBRK)
	{  
	  if((lval->auxFlags & 0x2) == 0x2)
	    {
	      if(lval->mapType != expressionType(placement->branch[2],CurrScope)->datatype)
		{
		  fprintf(stderr,"wrong map type\n");
		  exit(3);
		}
	      if(copyVal)
		{
		  if(copyVal->key)
		    free(copyVal->key);
		  free(copyVal);
		}
	      copyVal = malloc(sizeof(struct tokenHashList));
	      copyVal ->next = NULL;
	      copyVal->datatype = lval->datatype;
	      copyVal->key = NULL;
	      copyVal->auxFlags = 0x0;
	      return copyVal;
	    }
	  else if((lval->auxFlags & 0x40) == 0x40)
	    {
	      if(!areSameType(baseTypes[0],expressionType(placement->branch[2],CurrScope),0,0))
		{
		  fprintf(stderr,"array inside needs to be an int\n");
		  exit(3);		  
		}
	      if(copyVal)
		{
		  if(copyVal->key)
		    free(copyVal->key);
		  free(copyVal);
		}
	      
	      copyVal = malloc(sizeof(struct tokenHashList));
	      copyVal->next = NULL;
	      copyVal->datatype = lval->datatype;
	      copyVal->key = strdup(lval->key);
	      copyVal->auxFlags = lval->auxFlags-0x40;
	      copyVal->mapType = lval->mapType;
	      
	      return copyVal;
	    }
	  else
	    {
	      fprintf(stderr,"brackets not onmap or array \n");
	      exit(3);
	    }
	}
    }
  return NULL;
}



struct tokenHashList* expressionType(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  if(placement->prodrule == -1)
    {
      return getTypeFromLiteral(placement,CurrScope);
    }
  if(placement->prodrule == expr)
    {
      return expressionTypeCheck(placement,CurrScope);
    }
  else if(placement->prodrule == pexpr)
    {
      return expressionTypeCheck(placement->branch[1],CurrScope);
    }
  else if (placement->prodrule == pseudocall)
    {
      /*check arguments in fucntion are correct*/
      pseudocalTypeCheck(placement->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[0]->value->text)->head),GetScope(placement->branch[0]->value->text));
      /*check return type of function*/
      return typeOf(placement->branch[0],CurrScope);      
    }
  return NULL;
}

void caseOfUnaryExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope)
{
  if(placement->branch[0]->value->category == LSTAR)
    {
      if(debug)
	{
	  printf("searching for:%s in scope:%s\n",placement->branch[1]->value->text,CurrScope->scopeName);
	  fflush(stdout);
	}
      if(!areSameType(typeOf(placement->branch[1],CurrScope),GetListI(funcScope,depth),1,0))
	{
	  fprintf(stderr,"ERROR: line:%d file:%s argument %d was of type:*",placement->branch[1]->value->lineno,placement->filename,depth);
	  PrintType(typeOf(placement->branch[1],CurrScope));
	  fprintf(stderr,"  function parameter expected:");
	  PrintType(GetListI(funcScope,depth));
	  fprintf(stderr,"\n");
	  exit(3);
	}
      placement->dataType = typeOf(placement->branch[1],CurrScope);
    }
  else
    {
      caseOfSimpleExpression(placement->branch[1],CurrScope,depth,funcScope);
    }
}


void caseOfSimpleExpression(struct tokenTree* placement, struct ScopeList* CurrScope,int depth,struct ScopeList* funcScope)
{
  struct tokenHashList* lVal = NULL;
  if(placement->value->category == LIDENTIFIER)
    {
      lVal = typeOf(placement,CurrScope);
    }
  else if(placement->value->category == LLITERAL)
    {
      lVal=getTypeFromLiteral(placement,CurrScope);
    }
  if(debug)
    {
      printf("searching for:%s in scope:%s\n",placement->value->text,CurrScope->scopeName);
      fflush(stdout);
    }
  if(!areSameType(lVal,GetListI(funcScope,depth),0,0))
    {
      fprintf(stderr,"ERROR: line:%d file:%s argument %d was of type:",placement->value->lineno,placement->filename,depth);
      PrintType(lVal);
      fprintf(stderr,"  function parameter expected:");
      PrintType(GetListI(funcScope,depth));
      fprintf(stderr,"\n");
      exit(3);
    }
  if((lVal->auxFlags & 0x8) == 0x8)
    {
      fprintf(stderr, "Error: Function refered to as literal line:%d file:%s\n",placement->value->lineno,placement->filename);
    }
}


void buildSymbolTable(struct tokenTree* treeHead, struct ScopeList* CurrScope)
{
  /*simple checking although this is technically not possible if it passes Syntax analysis */
  if(treeHead->branch[0]->prodrule != package)
    {
      fprintf(stderr,"error package rule not found in file: %s\n", treeHead->filename);
    }
  /*depth first search on each tree to find  and create different scopes
    while also adding variables to their appropriate scopes */
  depthFirst(treeHead,CurrScope);
}


int SizeOf(struct ParameterList* LL)
{
  int s = 0;
  while(LL)
    {
      LL = LL->next;
      s++;
    }
  if(debug)
    printf("Size is: %d\n",s);
  return s;
}

void checkType(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  if(!placement)
    return;
  int i = 0;
  int endingi = placement->nBranches;
  if(placement ->prodrule == xfndcl)
    {
      CurrScope = GetScope(placement->branch[1]->branch[0]->value->text);
    }
  else if(placement->prodrule == pseudocall)
    {
      if(placement->branch[0]->prodrule == pexpr_no_paren)
	{
	  return; 
	}
      if(strcmp(placement->branch[0]->value->text,"make") == 0)
	{
	  return;
	}
      pseudocalTypeCheck(placement->branch[2],CurrScope,0,SizeOf(GetScope(placement->branch[0]->value->text)->head),GetScope(placement->branch[0]->value->text));
      return;
    }
  else if(placement->prodrule == expr)
    {
      expressionTypeCheck(placement,CurrScope);
      return;
      /*TODO: handle inside of this */
    }
    else if(placement->prodrule == simple_stmt)
    {
      simplestmtTypeCheck(placement,CurrScope);
      return;
      /* TODO */
    }
  else if(placement->prodrule == non_dcl_stmt)
    {
      nondclTypeCheck(placement,CurrScope);
      return;
      /*TODO: handle inside of this */
      /*return statement specifically check return type*/
    }
  for(; i < endingi; i++)
    {
      checkType(placement->branch[i],CurrScope);
    }

  return;
}

struct ScopeList* GetScope(char* scopename)
{
  struct ScopeList* tmp = ScopeListHead;
  while(strcmp(scopename,tmp->scopeName) != 0)
    {
      tmp= tmp->next;
    }
  return tmp;
  /*ERROR handling*/
}

struct ScopeList* ScopeFromTypeNumber(int type, struct tokenTree* placement)
{
  if(type < 4)
    {
      fprintf(stderr,"Error basetype cannot be dereferenced line:%d\n",placement->value->lineno);
      exit(3);
      /*TODO*/
    }
  int currType = 4;
  struct ScopeList* scope = ScopeListHead;
  while(scope)
    {
      if(scope->structType)
	{
	  
	  if(type == ++currType)
	    {
	      //printf("returning scope: %s\n", scope->scopeName);
	      return scope;
	    }	  
	}
      scope = scope->next;
    }
  return NULL;
}

struct tokenHashList* typeOf(struct tokenTree* placement/*I'm considering changing this to char* instead*/, struct ScopeList* CurrScope)
{
  struct tokenHashList* tmp = searchTable(CurrScope->tableVal,placement->value->text);
  if(tmp)
    return tmp;
  else
    return searchTable(ScopeListHead->tableVal,placement->value->text);
}

int areSameType(struct tokenHashList* x,struct tokenHashList* y, int xDereference, int yDereference)
{
  /*may have to adjust for implicit type changes*/
  if(x->datatype != y->datatype)
    {
      return 0;
    }
  if((x->auxFlags & 0x2) == 0x2)
    {
      if(x->mapType != y->mapType)
	{
	  return 0;
	}
    }
  /*dereferenced comes in here*/
  /*Both Have to Be Arrays or both not*/
  if((x->auxFlags & 0x40) != (y->auxFlags & 0x40))
    {
      if(((x->auxFlags & 0x40) == 0x40) && (xDereference))
	{

	}
      else if(((y->auxFlags & 0x40) == 0x40) && (yDereference))
	{

	}
      else
	{
	  return 0;
	}
    }

  /*Both Have to Be Pointers or both not */
  if((x->auxFlags & 0x4) != (y->auxFlags & 0x4))
    {
      if((x->auxFlags & 0x4) == 0x4 && xDereference)
	{

	}
      else if((y->auxFlags & 0x4) == 0x4 && yDereference)
	{

	}
      else
	{
	  return 0;
	}
    }

  return 1;
}

void depthFirst(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  int i= 0;

  /*checks if NULL*/
  if(!placement)
    return;
  int endingi = placement->nBranches;
  if(placement->prodrule == import)
    {
      if(strcmp(placement->branch[1]->value->text,"\"fmt\"") == 0)
	{
	  AddFmtToGlobal(CurrScope,placement);
	}
      else if(strcmp(placement->branch[1]->value->text,"\"rand\"") == 0)
	{
	  AddRandToGlobal(CurrScope,placement);
	}
      else if(strcmp(placement->branch[1]->value->text,"\"time\"") == 0)
	{
	  AddTimeToGlobal(CurrScope,placement);
	}
      else
	{
	  fprintf(stderr,"Library: %s not Supported in VGo. File:\"%s\" line:%d\n",placement->branch[1]->value->sval,placement->branch[1]->filename,placement->branch[1]->value->lineno);
	  exit(3);
	}
    }
  else if(placement->prodrule == fndcl)
    {
      i = 1;
      endingi--;
    }
  else if(placement->prodrule == pseudocall)
    {
      if(placement->branch[0]->prodrule != pexpr_no_paren)
	{
	  i = 2;
	}
    }
  else if(placement->prodrule == pexpr_no_paren)
    {
      if(placement->branch[1]->value)
	{
	  if(!strcmp(placement->branch[1]->value->text,"."))
	    {
	      return;
	    }
	}
    }
  else if(placement->prodrule == package)
    {
      if(strcmp(placement->branch[1]->value->text,"main") != 0)
	{
	  fprintf(stderr,"ERROR: vgo only supports package \"main\"\n");
	  exit(2);
	}
      return;
    }
  else if(placement->prodrule == common_dcl)
    {
      addCommonDeclarations(placement,CurrScope);
      return;
    }
  else if(placement->prodrule==oarg_type_list_ocomma)
    {
      addParameterValues(placement,CurrScope);
      return;
    }
  else if(placement->prodrule == xfndcl)
    {
      /*new function declaration
	As such new scope as well
      */
      if(debug)
	fprintf(stderr,"Entering New Scope: %s\n",placement->branch[1]->branch[0]->value->text);
      CurrScope = addNewScope(placement->branch[1]->branch[0]->value->text,0);
      if(placement->branch[1]->branch[7])
      	addToTable(ScopeListHead,placement->branch[1]->branch[0]->value,returnTypeBasedOnReservedword(placement->branch[1]->branch[7]->value),0x8,placement->filename,4);
      else if(placement->branch[1]->branch[4])
	{
	  int type = 4;
	  int auxflag = 0x8;
	  int mapType = 4;
	  if(placement->branch[1]->branch[4]->prodrule == -1)
	    {
	      type = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->value);
	    }
	  else if(placement->branch[1]->branch[4]->prodrule == othertype)
	    {
	      if(placement->branch[1]->branch[4]->branch[0]->value->category == LMAP)
		{
		  auxflag += 0x2;
		  mapType = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->branch[2]->value);
		  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->branch[4]->value);
		}
	      else if(placement->branch[1]->branch[4]->branch[0]->value->category == LLBRK)
		{
		  auxflag += 0x40;
		  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->branch[3]->value);
		}
	    }
	  else if(placement->branch[1]->branch[4]->prodrule == ptrtype)
	    {
	      auxflag += 0x4;
	      type = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->branch[1]->value);
	    }
	  addToTable(ScopeListHead,placement->branch[1]->branch[0]->value,type,auxflag,placement->filename,mapType);
	}
      else
	addToTable(ScopeListHead,placement->branch[1]->branch[0]->value,4,0x8,placement->filename,4);
    }
  else if( placement->prodrule == -1 && placement->value->category == 298/*LIDENTIFIER*/)
    {
      if(searchTable(ScopeListHead->tableVal,placement->value->text) ||
	 searchTable(CurrScope->tableVal,placement->value->text))
	{
	  //This is because I am too lazy to do DeMorgan's law and instead will just make an else statement and let that catch it below.
	}
      else
	{
	  fprintf(stderr,"%s: Error on line: %d, Variable: \"%s\" Undeclared at this scope\n",placement->filename,placement->value->lineno,placement->value->text);
	  exit(3);
	}
    }
  for(; i < endingi; i++)
    {
      depthFirst(placement->branch[i],CurrScope);
    }
}


int returnTypeBasedOnRightHandSide(struct token* value)
{
  int type = 4;
  if(value->ival != -10000)
    {
      type = 0;
    }
  else if(value->dval != NAN)
    {
      type =1;
    }
  else if(value->sval != NULL)
    {
      type =2;
    }
  else if(strcmp(value->text,"true") == 0 || strcmp(value->text,"false") == 0)
    {
      type =3;
    }
  return type;
}



int returnTypeBasedOnReservedword(struct token* value)
{
  int type = 4;
  switch(value->category)
    {
    case LINT:
      return 0;
      break;
    case LFLOAT:
      return 1;
      break;
    case LSTRING:
      return  2;
      break;
    case LBOOLEAN:
      return  3;
      break;
    }


  /*fun part*/
  /*return index in linked list + 4 of type*/
  struct ScopeList* CurrScope = ScopeListHead;
  int indexInList = 0;
  while(CurrScope)
    {
      if(CurrScope->structType)
	{
	  indexInList++;
	  if(!strcmp(CurrScope->scopeName,value->text))
	    {
	      return type+indexInList;
	    }
	}
      CurrScope = CurrScope->next;
    }
  return type;/* 4 reserved as idk type*/
}



void addCommonDeclarations(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  /*New Variable*/
  if(placement->branch[0]->value->category == LCONST)
    {
      if(placement->branch[1]->branch[2]->prodrule != expr)
	{
	  if(placement->branch[1]->branch[2]->value->category != LASOP)
	    {
	      /* const s = "string" */
	      int type = returnTypeBasedOnRightHandSide(placement->branch[1]->branch[2]->value);
	      addToTable(CurrScope,placement->branch[1]->branch[0]->value,type,0x4,placement->filename,4);
	    }
	  else
	    {
	      /* const s string = "string" */
	      int type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->value);
	      addToTable(CurrScope,placement->branch[1]->branch[0]->value,type,0x4,placement->filename,4);
	    }
	}
      else
	{
	  /* const s = expr*/
	  addToTable(CurrScope,placement->branch[1]->branch[0]->value,5,0x4,placement->filename,4);
	}
    }
  else if(placement->branch[0]->value->category == LVAR)
    {
      int auxflag = 0x0;
      int type;
      int mapType = 4;
      if(placement->branch[1]->branch[1]->prodrule == -1)
	type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->value);
      else if(placement->branch[1]->branch[1]->prodrule == othertype)
	{
	  if(placement->branch[1]->branch[1]->branch[0]->value->category == LMAP)
	    {
	      auxflag = 0x2;
	      type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->branch[4]->value);
	      mapType = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->branch[2]->value);
	    }
	  else if(placement->branch[1]->branch[1]->branch[0]->value->category == LLBRK)
	    {
	      auxflag = 0x40;
	      type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->branch[3]->value);
	    }
	}
      else if(placement->branch[1]->branch[1]->prodrule == ptrtype)
	{
	  auxflag = 0x4;
	  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->branch[1]->value);
	}
      if(placement->branch[1]->branch[0]->prodrule == dcl_name_list)
	{
	  dclNameListHandler(placement->branch[1]->branch[0],type,auxflag,mapType,CurrScope);
	}
      else
	{
	  addToTable(CurrScope,placement->branch[1]->branch[0]->value,type,auxflag,placement->filename,mapType);
	}

    }
  else if(placement->branch[0]->value->category == LTYPE)
    {
      newTypeDeclaration(placement->branch[1],CurrScope);
    }
  return;
}

void newTypeDeclaration(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  CurrScope = addNewScope(placement->branch[0]->value->text,1);
  structArgAdder(placement->branch[1]->branch[2],CurrScope);
}

void structArgAdder(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  if(!placement)
    return;
  if(placement->prodrule == structdcl)
    {
      if(placement->branch[0]->prodrule == new_name_list)
	{
	  newNameListHandler(placement->branch[0],placement->branch[1],CurrScope);
	}
      else
	{
	  int auxflag = 0x0;
	  int type;
	  int mapType = 4;
	  if(placement->branch[1]->prodrule == -1)
	    type = returnTypeBasedOnReservedword(placement->branch[1]->value);
	  else if(placement->branch[1]->prodrule == othertype)
	    {
	      if(placement->branch[1]->branch[0]->value->category == LMAP)
		{
		  auxflag = 0x2;
		  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->value);
		  mapType = returnTypeBasedOnReservedword(placement->branch[1]->branch[2]->value);
		}
	      else if(placement->branch[1]->branch[0]->value->category == LLBRK)
		{
		  auxflag = 0x40;
		  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[3]->value);
		}
	    }
	  else if(placement->branch[1]->prodrule == ptrtype)
	    {
	      auxflag = 0x4;
	      type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->value);
	    }
	  struct tokenHashList* tmp = addToTable(CurrScope,placement->branch[0]->value,type,auxflag,placement->filename,mapType);
	  addParameterList(tmp,CurrScope);
	}
    }
  else
    {
      structArgAdder(placement->branch[0],CurrScope);
      structArgAdder(placement->branch[2],CurrScope);
    }
}

void newNameListHandler(struct tokenTree* placement,struct tokenTree* type, struct ScopeList* CurrScope)
{
  if(placement->prodrule == -1)
    {
      int auxflag = 0x0;
      int Ntype;
      int mapType = 4;
      if(type->prodrule == -1)
	Ntype = returnTypeBasedOnReservedword(type->value);
      else if(type->prodrule == othertype)
	{
	  if(type->branch[0]->value->category == LMAP)
	    {
	      auxflag = 0x2;
	      Ntype = returnTypeBasedOnReservedword(type->branch[4]->value);
	      mapType = returnTypeBasedOnReservedword(type->branch[2]->value);
	    }
	  else if(type->branch[0]->value->category == LLBRK)
	    {
	      auxflag = 0x40;
	      Ntype = returnTypeBasedOnReservedword(type->branch[3]->value);
	    }
	}
      else if(type->prodrule == ptrtype)
	{
	  auxflag = 0x4;
	  Ntype = returnTypeBasedOnReservedword(type->branch[1]->value);
	}
      struct tokenHashList* tmp = addToTable(CurrScope,placement->value,Ntype,auxflag,placement->filename,mapType);
      addParameterList(tmp,CurrScope);
    }
  else
    {
      newNameListHandler(placement->branch[0],type,CurrScope);
      newNameListHandler(placement->branch[2],type,CurrScope);
    }
}

void argTypeHelper(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  int type = 4;
  int auxflag = 0x10;
  int mapType = 4;
  if(placement->branch[1]->prodrule == -1)
    {
      type = returnTypeBasedOnReservedword(placement->branch[1]->value);
      struct tokenHashList* tmp = addToTable(CurrScope,placement->branch[0]->value,type,auxflag,placement->filename,4);
      addParameterList(tmp,CurrScope);
    }
  else if(placement->branch[1]->prodrule == othertype)
    {
      if(placement->branch[1]->branch[0]->value->category == LMAP)
	{
	  auxflag += 0x2;
	  mapType = returnTypeBasedOnReservedword(placement->branch[1]->branch[2]->value);
	  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[4]->value);
	  struct tokenHashList* tmp = addToTable(CurrScope,placement->branch[0]->value,type,auxflag,placement->filename,mapType);
	  addParameterList(tmp,CurrScope);
	}
      else if(placement->branch[1]->branch[0]->value->category == LLBRK)
	{
	  auxflag += 0x40;
	  type = returnTypeBasedOnReservedword(placement->branch[1]->branch[3]->value);
	  struct tokenHashList* tmp = addToTable(CurrScope,placement->branch[0]->value,type,auxflag,placement->filename,mapType);
	  addParameterList(tmp,CurrScope);
	}
    }
  else if(placement->branch[1]->prodrule == ptrtype)
    {
      auxflag += 0x4;
      type = returnTypeBasedOnReservedword(placement->branch[1]->branch[1]->value);
      struct tokenHashList* tmp = addToTable(CurrScope,placement->branch[0]->value,type,auxflag,placement->filename,mapType);
      addParameterList(tmp,CurrScope);
    }
  /*argument passed has a type and name */
}


void addParameterValues(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  int evenNumbers;
  struct tokenHashList* tmp;
  /*PARAMETER*/
  if(placement->branch[0]->prodrule == arg_type_list)
    {
      if(placement->branch[0]->branch[1]->value->category != LCM)
	{
	  fprintf(stderr,"Jake U big messed up\n");
	  /*This is what industry standard error printing looks like*/
	  exit(3);
	}
      for(evenNumbers = 0; evenNumbers <=2; evenNumbers+=2)
	{
	  if(placement->branch[0]->branch[evenNumbers]->prodrule == arg_type_list)
	    {
	      addParameterValues(placement->branch[0],CurrScope);
	    }
	  else if(placement->branch[0]->branch[evenNumbers]->prodrule == arg_type)
	    {
	      argTypeHelper(placement->branch[0]->branch[evenNumbers],CurrScope);
	    }
	  else
	    {
	      tmp = addToTable(CurrScope,placement->branch[0]->branch[evenNumbers]->value,5,0x10,placement->filename,4);
	      addParameterList(tmp,CurrScope);
	      /* argumnet passed has no type */
	    }
	}
    }
  else if(placement->branch[0]->prodrule == arg_type)
    {
      argTypeHelper(placement->branch[0],CurrScope);
    }
  else
    {
      tmp = addToTable(CurrScope,placement->branch[0]->value,5,0x10,placement->filename,4);
      addParameterList(tmp,CurrScope);
      /* argumnet passed has no type */
    }
  return;
}


void AddFmtToGlobal(struct ScopeList* scope, struct tokenTree* value)
{
  static int done = 0;
  if(!done)
    {
      addToGlobal(scope,"fmt",4,0x20,value->filename);
      addToGlobal(scope,"Println",4,0x28,value->filename);
      addToGlobal(scope,"Printf",4,0x28,value->filename);
      addToGlobal(scope,"Scan",4,0x28,value->filename);
      addToGlobal(scope,"make",4,0x28,value->filename);
      done = 1;
    }

}

void AddRandToGlobal(struct ScopeList* scope, struct tokenTree* value)
{
  static int done = 0;
  if(!done)
    {
      addToGlobal(scope,"rand",4,0x20,value->filename);
      addToGlobal(scope,"Intn",4,0x28,value->filename);
      done = 1;
    }

}

void AddTimeToGlobal(struct ScopeList* scope, struct tokenTree* value)
{
  static int done = 0;
  if(!done)
    {
      addToGlobal(scope,"time",4,0x20,value->filename);
      addToGlobal(scope,"Now",4,0x28,value->filename);
      done = 1;
    }

}



void dclNameListHandler(struct tokenTree* placement,int type,int auxflag,int mapType, struct ScopeList* CurrScope)
{
  if(!placement)
    return;
  if(placement->branch[0]->prodrule == dcl_name_list)
    {
      dclNameListHandler(placement->branch[0],type,auxflag,mapType,CurrScope);
    }
  else if(placement->branch[0]->prodrule == -1)
    {
      addToTable(CurrScope,placement->branch[0]->value,type,auxflag,placement->filename,mapType);
    }
  addToTable(CurrScope,placement->branch[2]->value,type,auxflag,placement->filename,mapType);
}
