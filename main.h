#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <unistd.h>


struct token {
  int category;   /* the integer code returned by yylex */
  char *text;     /* the actual string (lexeme) matched */
  int lineno;     /* the line number on which the token occurs */
  int ival;       /* for integer constants, store binary value here */
  double dval;	  /* for real constants, store binary value here */
  char *sval;     /* for string constants, malloc space, de-escape, store */
                  /*    the string (less quotes and after escapes) here */
};


struct tokenTree
{
  struct threeAddress* first, *follow, *True, *False; //???
  struct threeAddress* place; //?
  struct codeList* code;
  int placeVal;
  char* filename;
  int prodrule;   /* 0 if leaf; prodrule number otherwise defined in prouductionRules.h */
  int nBranches;  
  struct token* value; /* NULL if not a leaf */
  struct tokenTree* branch[9];
  struct tokenHashList* dataType;
};




/*globals*/
char* Lfilename;
char lastid[256];
FILE* yyin;
struct tokenTree* head;
struct TreeList* TreeListHead;
struct ScopeList* ScopeListHead;


int lineNumber;
int debug;
int typeCheckIsOn;

/*functions */
int yylex();
int vgoExtensionHandler(char**,int);
int main(int argc, char* argv[]);
int ayyerror(const char* s,int);
int yyerror(const char* s);
void printIntToStringProdRule(int);

#endif
