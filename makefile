#
# vgo Makefile
#

IN=vgo
BISON=bison
LEX=flex
CC=gcc
CFLAGS=-g -Wall -Wextra -Werror

SOURCES=clex.l LinkedList.c main.c makefile cbison.y tree.c README intToStringProdRule.c hashTable.c semantic.c codeGeneration.c
HEADERS=LinkedList.h tree.h main.h productionRules.h hashTable.h semantic.h codeGeneration.h

.c.o:
	$(CC) -c $(CFLAGS) $<

vgo: clex.o main.o tree.o cbison.tab.o intToStringProdRule.o LinkedList.o hashTable.o semantic.o codeGeneration.o
	$(CC) -o $(IN) main.o clex.o cbison.tab.o tree.o intToStringProdRule.o LinkedList.o hashTable.o semantic.o codeGeneration.o -lm

all: 
	touch *.c *.l
	rm *.o clex.c
	make

codeGeneration.o: codeGeneration.c codeGeneration.h
	$(CC) -c $(CFLAGS) codeGeneration.c -lm

semantic.o: semantic.c semantic.h
	$(CC) -c $(CFLAGS) semantic.c


hashTable.o: hashTable.c hashTable.h
	$(CC) -c $(CFLAGS) hashTable.c

intToStringProdRule.o: intToStringProdRule.c
	$(CC) -c $(CFLAGS) intToStringProdRule.c

tree.o: tree.c tree.h cbison.tab.h main.h
	$(CC) -c $(CFLAGS) tree.c

cbison.tab.c cbison.tab.h:  main.h cbison.y
	$(BISON) -d -v  cbison.y 
#$(BISON) -d -v -W -t --debug cbison.y 
#
#

cbison.tab.o: cbison.tab.c main.h productionRules.h
	$(CC) $(CFLAGS) -c cbison.tab.c

clex.c: clex.l cbison.tab.h main.h
	$(LEX) -t clex.l >clex.c

clex.o: clex.c
	$(CC) -c $(CFLAGS) clex.c

main.o: main.c main.h codeGeneration.h
	$(CC) -c $(CFLAGS) main.c

LinkedList.o: LinkedList.c LinkedList.h main.h cbison.tab.h
	$(CC) -c $(CFLAGS) LinkedList.c

clean:
	rm -f *.o
	rm -f clex.c
	rm -f cbison.tab.h
	rm -f cbison.tab.c
	rm -f vgo
	rm -f cbison.output
	rm -f vgo.zip
zip:
	zip vgo.zip $(SOURCES) $(HEADERS)
