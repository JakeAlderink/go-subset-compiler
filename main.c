#include "cbison.tab.h"
#include "tree.h"
#include "LinkedList.h"
#include "semantic.h"
#include "codeGeneration.h"

extern char lastid[256];
extern char* Lfilename;
extern struct tokenTree* head;
extern int lineNumber;


bool printTheTree;
bool printTheTables;
bool printTheParameters;

int main(int argc, char *argv[])
{
  printTheTree= false;
  printTheTables = false;
  printTheParameters = false;
  typeCheckIsOn = true;
  TreeListHead = NULL;
   #if YYDEBUG == 1
           extern yydebug;
           yydebug = 1;
   #endif
  debug = 0;
  if (argc == 1)
    {
      printf("No input file specified\n");
      exit(0);
    }
  
  argc--;
  argv++;
  vgoExtensionHandler(argv,argc);
  
  /* int i = 0; */
  /* for(i = 0; i < argc; i++) */
  /*   { */
  /*     printf("%s\n",argv[i]); */
  /*   } */
  
  while(argv[0][0] == '-')
    {
      argc--;
      argv++;      
    }

  if (argc == 0)
    {
      printf("No input file specified\n");
      exit(0);
    }

  while(argc != 0 )
    {
      if(access(argv[0],F_OK)==-1)
	{
	  printf("invalid file %s\n", argv[0]);
	  exit(1);
	  continue;
	  
	}
      
      yyin = fopen(argv[0], "r");
      Lfilename = argv[0];

      /* while(1) */
      /* 	{ */
      /* 	  int yylexReturn = yylex(); */
      /* 	  if(yylexReturn == 0) */
      /* 	    break; */
      /* 	} */

      yyparse();
      addToLinkedList(); /*add tree created to linked list of trees*/
      argc--;
      argv++;
    }
  if(printTheTree)
     printFormattedLinkedList(); /*print out tree*/
  
  semantic(); 
  
  /*Semantic Analysis Performed*/
  if(printTheTables)
    {
      printFormattedScopeList();
    }
  if(printTheParameters)
    {
      printScopesParameters();
    }
  generateCode();
  
  
  return 0;
}

int vgoExtensionHandler(char** filename, int argc)
{
  /*handles the exntension memory management etc.*/
  /* currently if .go is not at the end it'll add it and that is the extent of its logic */
  int argumentI = 0;
  char * copy;
  for(argumentI =0; argumentI< argc; argumentI++)
    {
      if(filename[argumentI][0] != '-')
	{
	  bool utilized = false;
	  copy = strdup(filename[argumentI]);
	  if(strstr(copy,".go") == NULL)
	    {
	      copy = realloc(copy,(strlen(copy)+3)*sizeof(char*));
	      strcat(copy,".go");
	      utilized = !utilized;
	      filename[argumentI] = copy;
	    }
	  if(!utilized)
	    {
	      free(copy);
	    }
	}
      else
	{
	  if(strcmp(filename[argumentI],"-tree") == 0)
	    {
	      printTheTree = true;
	    }
	  else if(strcmp(filename[argumentI],"-symtab") == 0)
	    {
	      printTheTables = true;
	    }
	  else if(strcmp(filename[argumentI],"-param") == 0)
	    {
	      printTheParameters = true;
	    }
	  else if(strcmp(filename[argumentI],"-all") == 0)
	    {
	      printTheTables = true;
	      printTheParameters = true;
	      printTheTree = true;
	      typeCheckIsOn = true;
	    }
	  else if(strcmp(filename[argumentI],"-check")==0)
	    {
	      typeCheckIsOn = true;
	    }
	  else if(strcmp(filename[argumentI],"-debug")==0)
	    {
	      debug = true;
	    }
	  else
	    {
	      printf("Error unknown option %s\n",filename[argumentI]);
	      exit(1);
	    }
	}
    }
  return 0;
}

int yywrap()
{
  return -1;
}
