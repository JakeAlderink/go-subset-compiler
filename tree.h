#ifndef TREE_H
#define TREE_H

#include "main.h"
#include "cbison.tab.h"

void AddToTree(int category);
int printTree(struct tokenTree*);
struct tokenTree* AllocateTreeLeafNode(int);
struct tokenTree* AllocateTreeProductionNode(int,int);

#endif
