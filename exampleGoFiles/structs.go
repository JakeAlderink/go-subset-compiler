package main

import "fmt"

type person struct {
    name string
    age  int
}

func NewPerson(name string) person {
    var p person
    p.name = name
    p.age = 42
    return p
}

func main() {

    fmt.Println(NewPerson("Jon"))



}