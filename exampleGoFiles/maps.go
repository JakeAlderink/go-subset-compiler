package main

import "fmt"

func main() {
    var m map[string]int
    m = make(map[string]int)

    m["k1"] = 7
    m["k2"] = 13

    fmt.Println("map:", m)
    var v1 map[string]int
    
    v1 = m["k1"]
    fmt.Println("v1: ", v1)

    fmt.Println("len:", len(m))

    delete(m, "k2")
    fmt.Println("map:", m)

    var prs map[string]int
    _, prs = m["k2"]
    fmt.Println("prs:", prs)
    var n map[string]int
    
    fmt.Println("map:", n)
}