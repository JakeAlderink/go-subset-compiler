package main

import "fmt"

var res1 int
var res float64

func plus(a int, b int) int {

    return a + b
}

func plusPlus(a, b, c int) int {
     res = 18
     return a + b + c
}

func main() {
    var res int

    res = plus(1, 2)
    fmt.Println("1+2 =", res)
    var ret float

    
    const tippy = "Hello"
    res = plusPlus(1, 2, 3)
    fmt.Println("1+2+3 =", res)
}