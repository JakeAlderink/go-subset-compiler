package main

import "fmt"
import "time"
import "rand"

type person struct{
  age,man,dude,value,kid int
  name string
  german bool
  french float64
}

type nperson struct{
  german bool
  french float64
  dat map[int]int
  ats [10]int
  p *int
}

var e int

type notperson struct{
     name string
     x nperson
}

func main(){

     var x *int
     var y person
     var z nperson
     var a notperson
     var b whatperson
     var c int
     var d int
     var f [10]string
     var g [10]int
     var h string
     var i float64
     var j *nperson
     var k map[string]notperson
     
     d = 0
     c = 3
     e = c + d
     d = c + d + e
     //simple_stmt -> expr

     Arg0()
     d = c+Arg1(c+1)+4
     g = typeReturn(x,f)
     //d = typeReturn(x,f)[1]
     d = Arg3(g[1],h,i)
     d = Arg3(*x,h,i)
     j = TypeReturn(j,k)
     TypesArgs(h,Arg1(Arg0()))
     d++;
//     d = Arg1(k["help"].x.ats[1]);
}

func typeReturn(val *int, val1 [10]string) [10]int {
     var x [10]int
     return x
}

func TypeReturn(val *nperson, val1 map[string]notperson) *nperson{
	return val
}

func TypesArgs(val string,val1 int){
     var x, y, z int
     var a [32]int
}

func Arg1(val int) int{
     return val
}

func Arg3(val int,val1 string, val2 float64) int{
     return val
}

func Arg0()int{
     return 1+1
}
