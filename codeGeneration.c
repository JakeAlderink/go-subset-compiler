#include "codeGeneration.h"
#include <math.h>
#include "productionRules.h"
#include "hashTable.h"

int offset;
int currRegion;
int procedureIndex;

char* genTacFile();
void labelPocalypse(struct tokenTree* placement);
void printListDebug();
struct threeAddress* makeTACinstance(int a,int b);
void createFollows(struct tokenTree* placement);
struct threeAddress* makePlace(struct tokenHashList* value);
void simpleUnaryExpression(struct tokenTree* placement, int opcode);
void expressionHandler(struct tokenTree* placement);
void uexpressionHandler(struct tokenTree* placement);
void simpleStatementHandler(struct tokenTree* placement);
void labelRecursive(struct tokenTree* placement, struct ScopeList* CurrScope);
void printTAC(struct codeList* subject, char* filename);
int sizeThisHash(struct tokenHashList* tmp);
void assignScopeSizes();
void assignScope(struct ScopeList* scope);
void assignPlaceValues();
char *opcodeTrans(struct codeList* passed);
void simpleOperator(struct tokenTree* placement, int opcode);
void createFirsts(struct tokenTree* placement);
void pseudocallHandler(struct tokenTree* placement);
void addAllFuncScopes();



void labelPocalypse(struct tokenTree* placement)
{
  
  for(int i = 0; i < placement->nBranches; i++)
    {
    }
}

struct codeList* genCodeInstance(int opcode, struct threeAddress* destination, struct threeAddress* source, struct threeAddress* source1)
{
  struct codeList* temp = (struct codeList*)malloc(sizeof(struct codeList));
  temp->opcode = opcode;
  temp->destination = destination;
  temp->source = source;
  temp->source1 = source1;
  temp->next = NULL;
  return temp;
}

void addToList(int val)
{
  if(!priorityListHead)
    {
      priorityListHead = malloc(sizeof(struct priorityList));
      priorityListHead->next = NULL;
      priorityListHead->labelVal = val;
      return;
    }
  else
    {
      struct priorityList* temp = malloc(sizeof(struct priorityList));
      temp->next = priorityListHead;
      temp->labelVal = val;
      priorityListHead = temp;
    }
}

int isIn(int val)
{
  struct priorityList* temp = priorityListHead;
  while(temp)
    {
      if(temp->labelVal == val)
	return 1;
      temp = temp->next;
    }
  return 0;
}


char* genTacFile()
{
  char* temp = strdup(Lfilename);
  if(!temp)
    exit(4);
  int i;
  for( i= strlen(temp)-1; i >=0 ; i--)
    {
      if(temp[i] == '.')
	break;
    }
  temp[++i] = 't';
  temp[++i] = 'a';
  temp = realloc(temp,strlen(temp)+2);
  temp[++i] = 'c';
  temp[++i] = '\0';
  return temp;
}


void generateCode()
{
  priorityListHead = NULL;
  funcListHead = NULL;
  stringRegionByteSize = 0;
  stringCodeList =0;
  currRegion = GLOBAL_REGION;
  procedureIndex = 0;
  offset = 0;
  codeHead = NULL;
  assignScopeSizes();
  addAllFuncScopes();
  assignPlaceValues();
  char* TacFileName = genTacFile();
  /*printTAC(codeHead,TacFileName);*/
  printTAC(codeHead,NULL);
  printf("%s\n",TacFileName);
}




void assignPlaceValues()
{
  createFirsts(TreeListHead->treeHead);
  createFollows(TreeListHead->treeHead);
  labelRecursive(TreeListHead->treeHead, ScopeListHead);
  codeHead = TreeListHead->treeHead->code;
}


void createFirsts(struct tokenTree* placement)
{
  if(!placement)
    return;
  int i;
  for( i = 0; i < placement->nBranches;i++)
    createFirsts(placement->branch[i]);
  switch(placement->prodrule)
    {
    case simple_stmt:
    case compound_stmt:
    case stmt_list:
    case for_stmt:
    case if_stmt:
      placement->first = newLabel();
      break;
    }
}


void createFollows(struct tokenTree* placement)
{
  if(!placement)
    return;
  if(placement->prodrule == stmt_list)
    {
      if(placement->branch[2])
	{
	  placement->branch[0]->follow = placement->branch[2]->first;
	  placement->branch[2]->follow = placement->follow;
	}
      else
	{
	  placement->branch[0]->follow = placement->follow;
	}
    }
  else if(placement->prodrule == xfndcl)
    {
      if(!placement->branch[2]->follow)
	placement->branch[2]->follow = newLabel();
    }
  else if(placement->prodrule == compound_stmt)
    {
      if(placement->branch[1] && !placement->branch[1]->follow)
	placement->branch[1]->follow = placement->follow;
    }
  int i = 0;
  for(; i < placement->nBranches; i++)
    createFollows(placement->branch[i]);
}


void labelRecursive(struct tokenTree* placement, struct ScopeList* CurrScope)
{
  if(!placement)
    return;
  if(placement->prodrule == xfndcl)
    {
      CurrScope = GetScope(placement->branch[1]->branch[0]->value->text);
      offset = CurrScope->scopeSize;
      /*change region*/
      /*change offset to end of scopes offset*/
    }
  if(placement->prodrule == common_dcl)
    {
      return;
    }
  for(int i = 0; i < placement->nBranches; i++)
    {
      labelRecursive(placement->branch[i], CurrScope);
    }
  if(placement->prodrule == -1)
    {
      if(placement->value->category == LLITERAL)
	{
	  if(placement->value->ival != -10000)
	    {
	      placement->place = makeTACinstance(CONST_REGION,placement->value->ival);
	    }
	  else if(placement->value->sval != NULL)
	    {
	      addToStringRegion(placement->value->text);
	      placement->place = makeTACinstance(STRING_REGION,findOffset(placement->value->text));
	    }
	  else
	    placement->place = makeTACinstance(CONST_REGION,placement->value->dval);
	}
      else if(placement->value->category == LIDENTIFIER)
	{
	  placement->place = makePlace(typeOf(placement,CurrScope));
	}
    }
  else if(placement->prodrule == fnbody)
    {
      if(placement->branch[1])
	placement->branch[1]->follow = newLabel();
      placement->code = concat(placement->branch[1]->code,genCodeInstance(LABEL,placement->branch[1]->follow,NULL,NULL));
      placement->code = concat(placement->code,genCodeInstance(RET_OP,NULL,NULL,NULL));
    }
  else if(placement->prodrule == stmt_list)
    {
      placement->code = concat(placement->code,placement->branch[0]->code);
      //placement->code = concat(placement->code,genCodeInstance(LABEL,placement->branch[0]->follow,NULL,NULL));
      if(placement->branch[2])
	{
	  placement->code = concat(placement->code,placement->branch[2]->code);
	  placement->code = concat(placement->code,genCodeInstance(LABEL,placement->branch[2]->follow,NULL,NULL));
	}
    }
  else if(placement->prodrule == compound_stmt)
    {
      
    }
  else if(placement->prodrule == expr)
    {
      expressionHandler(placement);
    }
  else if(placement->prodrule == uexpr)
    {
      uexpressionHandler(placement);
    }
  else if(placement->prodrule == pexpr)
    {
      placement->place = placement->branch[0]->place;
      placement->code = placement->branch[0]->code;
    }
  else if(placement->prodrule == simple_stmt)
    {
      simpleStatementHandler(placement);
    }
  else if(placement->prodrule == if_stmt)
    {
      if(!placement->branch[1]->True)
	placement->branch[1]->True = newLabel();
      placement->branch[1]->False = placement->follow;
      placement->branch[2]->follow = placement->follow;
      placement->code = concat(placement->branch[1]->code,genCodeInstance(LABEL,placement->branch[1]->True,NULL,NULL));
      placement->code = concat(placement->code,placement->branch[2]->code);
    }
  else if(placement->prodrule == elseif)
    {
      /*TODO*/
    }
  else if(placement->prodrule == xfndcl)
    {
      CurrScope->scopeSize = offset;
      offset = 0;
      placement->code = concat(genCodeInstance(PROC_DEC,makeTACinstance(0,getFuncIndex(funcListHead,placement->branch[1]->branch[0]->value->text)),makeTACinstance(0,getFuncParamSize(CurrScope)),makeTACinstance(0,CurrScope->scopeSize)),placement->branch[2]->code);
    }
  else if(placement->prodrule == elsesss)
    {
      /*TODO*/
    }
  else if(placement->prodrule == for_body)
    {
      if(placement->branch[0]->prodrule == for_header)
	{
	  /*typical for stmt*/
	  if(!placement->branch[0]->branch[2]->True)
	    placement->branch[0]->branch[2]->True = newLabel();
	  if(!placement->branch[0]->branch[2]->first)
	    placement->branch[0]->branch[2]->first = newLabel();
	  placement->branch[0]->branch[2]->follow = placement->follow;
	  placement->branch[1]->follow = placement->branch[0]->branch[4]->first;
	  placement->code = concat(placement->branch[0]->branch[0]->code,genCodeInstance(LABEL,placement->branch[0]->branch[2]->first,NULL,NULL));
	  placement->code = concat(placement->code,placement->branch[0]->branch[2]->code);
	  placement->code = concat(placement->code,genCodeInstance(LABEL,placement->branch[0]->branch[2]->True,NULL,NULL));
	  placement->code = concat(placement->code,placement->branch[1]->code);
	  placement->code = concat(placement->code,placement->branch[0]->branch[4]->code);
	  if(debug){
	    printf("GOTO goes to prodrule:");
	    printIntToStringProdRule(placement->branch[0]->branch[2]->prodrule);
	    printf("\n");
	  }
	  placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->branch[0]->branch[2]->first,NULL,NULL));
	}
      else
	{
	  if(!placement->branch[0]->True)
	    placement->branch[0]->True  = newLabel();
	  if(!placement->branch[0]->first)
	    placement->branch[0]->first = newLabel();
	  placement->branch[0]->False = placement->follow;	  
	  placement->branch[1]->follow=placement->branch[0]->first;
	  placement->code = concat(genCodeInstance(LABEL,placement->branch[0]->first,NULL,NULL),placement->branch[0]->code);
	  placement->code = concat(placement->code,genCodeInstance(LABEL,placement->branch[0]->True,NULL,NULL));
	  placement->code = concat(placement->code,placement->branch[1]->code);
	  if(debug){
	    printf("GOTO goes to prodrule:");
	    printIntToStringProdRule(placement->branch[0]->prodrule);
	    printf("\n");
	  }
	  placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->branch[0]->first,NULL,NULL));
	}
    }
  else if(placement->prodrule == pseudocall)
    {
      pseudocallHandler(placement);
    }
  else
    {
      int i =0;
      for( i=0; i < placement->nBranches; i++)
	{
	  if(placement->branch[i])
	    placement->code = concat(placement->code,placement->branch[i]->code);
	}
      /*first follow lecture 43*/
    }
}


struct threeAddress* makeTACinstance(int a,int b)
{
  struct threeAddress* temp = malloc(sizeof(struct threeAddress));
  temp->region = a;
  temp->offset = b;
  return temp;
}


void pseudocallHandler(struct tokenTree* placement)
{
  placement->place = NULL;
  int i = 0;
  if(placement->branch[1]->prodrule == LRPAR)
    {
      //SizeOf(GetScope(placement->branch[0]->value->text));
      /*NO ARGS*/
    }
  else
    {
      for(; i < SizeOf(GetScope(placement->branch[0]->value->text)->head);i++)
	placement->code = concat(placement->code,genCodeInstance(PARM_OP,NULL,NULL,NULL));
    }
  placement->code = concat(placement->code,genCodeInstance(CALL_OP,makeTACinstance(0,getFuncIndex(funcListHead,placement->branch[0]->value->text)),makeTACinstance(0,SizeOf(GetScope(placement->branch[0]->value->text)->head)),placement->follow));
}

void addAllFuncScopes()
{
  struct ScopeList* temp = ScopeListHead;
  while(temp)
    {
      if(strcmp(temp->scopeName,"Global Scope") == 0 ||  temp->structType)
	{
	}
      else
	{
	  addFuncName(temp->scopeName);
	}
      temp = temp->next;
    }
}

struct threeAddress* makePlace(struct tokenHashList* value)
{
  struct threeAddress *temp = malloc(sizeof(struct threeAddress));
  temp->region = LOCAL_REGION;
  temp->offset = value->offset;
  return temp;
}


void uexpressionHandler(struct tokenTree* placement)
{
  switch(placement->branch[0]->value->category)
    {
    case LPLUS:
      simpleUnaryExpression(placement, UADD_OP);
      break;
    case LMINUS:
      simpleUnaryExpression(placement, USUB_OP);
      break;
    case LSTAR:
      simpleUnaryExpression(placement, USTAR_OP);
      break;
    }
}


void simpleUnaryExpression(struct tokenTree* placement, int opcode)
{
  placement->place = newTemp();
  placement->code  = concat(placement->branch[1]->code, genCodeInstance(opcode, placement->place, placement->branch[1]->place, NULL));
}


void expressionHandler(struct tokenTree* placement)
{
  switch(placement->branch[1]->value->category)
    {
    case LANDAND:
      if(!placement->branch[2]->first)
	placement->branch[2]->first = newLabel();
      placement->branch[0]->True = placement->branch[2]->first;
      placement->branch[0]->False = placement->False;
      placement->branch[2]->True = placement->True;
      placement->branch[2]->False = placement->False;
      placement->code = concat(placement->branch[0]->code,genCodeInstance(LABEL,placement->branch[2]->first,NULL,NULL));
      placement->code = concat(placement->code,placement->branch[2]->code);
      break;
    case LOROR:
      if(!placement->branch[2]->first)
	placement->branch[2]->first = newLabel();
      placement->branch[0]->True = placement->True;
      placement->branch[0]->False = placement->branch[2]->first;
      placement->branch[2]->True = placement->True;
      placement->branch[2]->False = placement->False;
      placement->code = concat(placement->branch[0]->code,genCodeInstance(LABEL,placement->branch[2]->first,NULL,NULL));
      placement->code = concat(placement->code,placement->branch[2]->code);	
      break;
    case LEQ:
      placement->place = newTemp();
      placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
      placement->code = concat(placement->code,genCodeInstance(BEQ_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(BIF_OP,placement->place,placement->True,NULL));
      placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->False,NULL,NULL));
      break;
    case LNE:
      placement->place = newTemp();
      placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
      placement->code = concat(placement->code,genCodeInstance(BNE_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(BIF_OP,placement->place,placement->True,NULL));
      placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->False,NULL,NULL));
      break;      
    case LLT:
      placement->place = newTemp();
      placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
      placement->code = concat(placement->code,genCodeInstance(BLT_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(BIF_OP,placement->place,placement->True,NULL));
      placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->False,NULL,NULL));
      break;
    case LLE:
      placement->place = newTemp();
      placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
      placement->code = concat(placement->code,genCodeInstance(BLE_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(BIF_OP,placement->place,placement->True,NULL));
      placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->False,NULL,NULL));
      break;
    case LGE:
      placement->place = newTemp();
      placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
      placement->code = concat(placement->code,genCodeInstance(BGE_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(BIF_OP,placement->place,placement->True,NULL));
      placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->False,NULL,NULL));
      break;
    case LGT:
      placement->place = newTemp();
      placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
      placement->code = concat(placement->code,genCodeInstance(BGT_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(BIF_OP,placement->place,placement->True,NULL));
      placement->code = concat(placement->code,genCodeInstance(GOTO_OP,placement->False,NULL,NULL));
      break;
    case LSTAR:
      simpleOperator(placement,MUL_OP);
      break;
    case LDIVIDE:
      simpleOperator(placement,DIV_OP);
      break;
    case LMOD:
      simpleOperator(placement,MOD_OP);
      break;
    case LPLUS:
      simpleOperator(placement,ADD_OP);
      break;
    case LMINUS:
      simpleOperator(placement,SUB_OP);
      break;
    case LOR:
    case LAND:
    case LANDNOT:
    case LLSH:
    case LRSH:
      break;
    }
}


void simpleOperator(struct tokenTree* placement, int opcode)
{
  placement->place = newTemp();
  placement->code = concat(placement->branch[0]->code,placement->branch[2]->code);
  placement->code = concat(placement->code,genCodeInstance(opcode,placement->place,placement->branch[0]->place,placement->branch[2]->place));
}


void simpleStatementHandler(struct tokenTree* placement)
{
  switch(placement->branch[1]->value->category)
    {
    case LASOP:
      placement->place = placement->branch[0]->place;
      placement->code = concat(placement->branch[2]->code,genCodeInstance(ASN_OP,placement->branch[0]->place,placement->branch[2]->place,NULL));
      break;
    case LMINUSEQ:
      placement->place = newTemp();
      placement->code = concat(placement->branch[2]->code,genCodeInstance(SUB_OP,placement->place,placement->branch[0]->place,placement->branch[2]->place));
      placement->code = concat(placement->code,genCodeInstance(ASN_OP,placement->branch[2]->place,placement->place,NULL));
      break;
    case LPLUSEQ:
      placement->place = newTemp();
      placement->code = concat(placement->branch[2]->code,genCodeInstance(ADD_OP,placement->place,placement->branch[2]->place,placement->branch[0]->place));
      placement->code = concat(placement->code,genCodeInstance(ASN_OP,placement->branch[2]->place,placement->place,NULL));
      break;
    case LINC:
      placement->place = newTemp();
      placement->code = concat(placement->code,genCodeInstance(ADD_OP,placement->place,placement->branch[0]->place,makeTACinstance(CONST_REGION,1)));
      break;
    case LDEC:
      placement->place = newTemp();
      placement->code = concat(placement->code,genCodeInstance(SUB_OP,placement->place,placement->branch[0]->place,makeTACinstance(CONST_REGION,1)));
      break;
    }
}


void printTAC(struct codeList* subject, char* filename)
{
  FILE* fptr;
  if(filename)
    {
      fptr = fopen(filename,"w");
    }
  else
    {
      fptr = stdout;
    }
  fprintf(fptr,".string:\n");
  printStrings(fptr);
  struct codeList* temp= subject;
  fprintf(fptr,".code:\n");
  while(temp)
    {
      char* val = opcodeTrans(temp);
      fprintf(fptr,"%s\n",val);
      free(val);
      temp= temp->next;
    }
}


int /*log10(num)*/baseOf(int num)
{
  if(!num)
    return 1;
  int sum = 0;
  while(num != 0)
    {
      sum ++;
      num/=10;
    }
  return sum;
}


char *opcodeTrans(struct codeList* passed)
{
  char* x;
  char* temp;
  switch(passed->opcode)
    {
    case LABEL:
      if(!passed->destination)
	return strdup("LABEL NULL:");
      x = (char*)malloc(6+baseOf(passed->destination->offset)+1);
      x[0] = 'L';
      x[1] = 'A';
      x[2] = 'B';
      x[3] = 'E';
      x[4] = 'L';
      x[5] = ' ';
      int i = 0;
      int tmp = passed->destination->offset;
      for(i = 0; i < baseOf(passed->destination->offset); i++)
	{
	  int innertemp = tmp/(pow(10,baseOf(passed->destination->offset)-i-1));
	  x[6+i] =  innertemp + '0';
	  tmp -= innertemp * pow(10,baseOf(passed->destination->offset)-i-1);
	}
      x[i+5+1] = '\0';
      return x;
    case RET_OP:
      return strdup("\tRET");
      break;
    case ADD_OP:
      x = strdup("\tadd loc:\0");
      break;
    case SUB_OP:
      x = strdup("\tsub loc:\0");
      break;
    case MUL_OP:
      x = strdup("\tmul loc:\0");
      break;
    case DIV_OP:
      x = strdup("\tdiv loc:\0");
      break;
    case ASN_OP:
      x = strdup("\tmove loc:\0");
      break;
    case MOD_OP:
      x = strdup("\tmod loc:\0");
      break;
    case USTAR_OP:
      x = strdup("\tderef loc:\0");
      break;
    case USUB_OP:
      x = strdup("\tneg loc:\0");
      break;
    case UADD_OP:
      x = strdup("\tpos loc:\0");
      break;
    case BIF_OP:
      x = strdup("\tif loc:\0");
      break;
    case BLT_OP:
      x = strdup("\tlt loc:\0");
      break;
    case BNE_OP:
      x = strdup("\tne loc:\0");
      break;
    case BLE_OP:
      x = strdup("\tle loc:\0");
      break;
    case BNIF_OP:
      x = strdup("\tbnif loc:\0");
      break;
    case BGE_OP:
      x = strdup("\tge loc:\0");
      break;
    case BGT_OP:
      x = strdup("\tgt loc:\0");
      break;
    case GOTO_OP:
      x = strdup("\tGOTO LABEL:\0");
      if(!passed->destination)
	{
	  temp = strdup("NO LABEL\0");
	  char *temps = myStrCat(x,temp);
	  free(x);
	  free(temp);
	  return temps;
	}
      break;
    case PARM_OP:
      x = strdup("\tparm loc:\0");
      break;
    case CALL_OP:
    case PROC_DEC:
      if(passed->opcode == PROC_DEC)
	x = strdup("proc \0");
      else
	x = strdup("\tCALL \0");
      temp = getFuncName(funcListHead,passed->destination->offset);
      temp = myStrCat(x,temp);
      free(x);
      x = myStrCat(temp,",");
      free(temp);
      temp = malloc(baseOf(passed->source->offset)+2);
      snprintf(temp,baseOf(passed->source->offset)+1,"%d", passed->source->offset);
      char* temps = myStrCat(x,temp);
      free(temp);
      free(x);
      if(!passed->source1)
	return temps;
      temp = temps;
      x = myStrCat(temp,",");
      free(temp);
      temp = malloc(baseOf(passed->source->offset)+2);
      snprintf(temp,baseOf(passed->source1->offset)+1,"%d", passed->source1->offset);
      temps = myStrCat(x,temp);
      free(temp);
      free(x);
      return temps;
      break;
    default:
      x = NULL;
    }
  if(!x)
    return NULL;
  temp = malloc(baseOf(passed->destination->offset)+2);
  snprintf(temp,baseOf(passed->destination->offset)+1,"%d", passed->destination->offset);
  char* temps = myStrCat(x,temp);
  free(temp);
  free(x);
  x = temps;
  if(passed->source)
    {
      temp = (char*)malloc(baseOf(passed->source->offset)+6);
      if(passed->source->region == CONST_REGION)
	{
	  strncpy(temp," con:",6);
	}
      else if(passed->source->region == LOCAL_REGION)
	{
	  strncpy(temp," loc:",6);
	}
      else if(passed->source->region == STRING_REGION)
	{
	  strncpy(temp," str:",6);
	}
      snprintf(temp+5,baseOf(passed->source->offset)+1,"%d",passed->source->offset);
      temps = myStrCat(x,temp);
      free(temp);
      free(x);
      x = temps;
    }
  if(passed->source1)
    {
      temp = (char*)malloc(baseOf(passed->source1->offset)+6);
      if(passed->source1->region == CONST_REGION)
	{
	  strncpy(temp," con:",6);
	}
      else if(passed->source1->region == LOCAL_REGION)
	{
	  strncpy(temp," loc:",6);
	}
      else if(passed->source1->region == STRING_REGION)
	{
	  strncpy(temp," str:",6);
	}
      snprintf(temp+5,baseOf(passed->source1->offset)+1,"%d",passed->source1->offset);
      temps = myStrCat(x,temp);
      free(temp);
      free(x);
      x = temps;
    }
  return x;
}


struct codeList* copyCode(struct codeList* subject)
{
  if(!subject)
    return NULL;
  struct codeList* copy= genCodeInstance(subject->opcode,subject->destination,subject->source,subject->source1);
  copy->next = copyCode(subject->next);
  return copy;
}


struct codeList* concat(struct codeList* firstC, struct codeList* secondC)
{
  struct codeList* temp = firstC;
  if(temp == NULL)
    {
      return secondC;
    }
  while(temp->next != NULL)
    temp = temp->next;
  temp->next = secondC;
  return firstC;
}


struct threeAddress* newTemp()
{
  struct threeAddress* temp = (struct threeAddress*)malloc(sizeof(struct threeAddress));
  temp->region = LOCAL_REGION;
  temp->offset = offset;
  offset += 8;
  return temp;
}


struct threeAddress* newLabel()
{
  static int counter = 1;
  struct threeAddress* destination = malloc(sizeof(struct threeAddress));
  destination->region = LABEL;
  destination->offset = counter;
  counter++;
  return destination;
}





void assignScopeSizes()
{
  struct ScopeList* tmp = ScopeListHead;
  while(tmp)
    {
      assignScope(tmp);
      if(debug)
	printf("%d\n",tmp->scopeSize);
      tmp = tmp->next;
    }
}


void assignScope(struct ScopeList* scope)
{
  int sum = 0;
  int i = 0;
  for(;i < Size; i++)
    {
      if(scope->tableVal[i])
	{
	  struct tokenHashList* tmp = scope->tableVal[i]->head;
	  while(tmp)
	    {
	      tmp->offset = sum;
	      sum += sizeThisHash(tmp);
	      tmp = tmp->next;
	    }
	}
    }
  scope->scopeSize = sum;
}


int sizeThisHash(struct tokenHashList* tmp)
{
  if((tmp->auxFlags & 0x2) == 0x2 || (tmp->auxFlags & 0x8) == 0x8)
    {
      return 0;
      /*Map so maybe dynamic memory?*/
    }
  if((tmp->auxFlags & 0x40) == 0x40)
    {
      tmp->byteSize = (8*tmp->arraySize);
      return 8*tmp->arraySize;
    }
  tmp->byteSize = 8;
  return 8;

}


char* getFuncName(struct funcList* curr,int index)
{
  struct funcList* temp = curr;
  int i;
  for( i= 0; i < index; i++)
    {
      temp = temp->next;
    }
  return temp->funcName;
}


void addFuncName(char* name)
{
  struct funcList* temp = funcListHead;
  if(!temp)
    {
      funcListHead = malloc(sizeof(struct funcList));
      funcListHead->funcName = strdup(name);
      funcListHead->next = NULL;
      return;
    }
  while(temp->next != NULL)
    {
      temp = temp->next;
    }
  temp->next = malloc(sizeof(struct funcList));
  temp->next->funcName = strdup(name);
  temp->next->next = NULL;
}

void printListDebug()
{
  struct funcList* temp = funcListHead;
  while(temp != NULL)
    {
      printf("%s\n",temp->funcName);
      temp = temp->next;
    }
  printf("\n");
  
}

int getFuncIndex(struct funcList* curr, char* name)
{
  if(!curr)
    return -1;
  struct funcList* temp = curr;
  int i =0;
  while(temp)
    {
      if(strcmp(name,temp->funcName) == 0)
	return i;
      temp = temp->next;
      i++;
    }
  return -1;
}


int getFuncParamSize(struct ScopeList* curr)
{
  struct ParameterList* temp = curr->head;
  int byteSize = 0;
  while(temp)
    {
      byteSize += temp->value->byteSize;
      temp = temp->next;
    }
  return byteSize;
}


char* myStrCat(char* s,char* t)
{
  if(debug)
    printf("string:%s string length:%zu string:%s string length:%zu\n",s,strlen(s),t,strlen(t));
  char* temp = malloc(sizeof(char)*(strlen(s) + strlen(t)+1));
  strncpy(temp,s,strlen(s));
  strncpy(temp+strlen(s),t,strlen(t));
  temp[strlen(s)+strlen(t)] = '\0';
  return temp;
}

void addString(char* str)
{
  int off = 0;
  struct strList* temp = stringCodeList;
  if(!temp)
    {
      stringCodeList = malloc(sizeof(struct strList));
      stringCodeList->funcName = strdup(str);
      stringCodeList->next = NULL;
      stringCodeList->offset = off*8;
      return;
    }
  while(temp->next != NULL)
    {
      temp = temp->next;
      off++;
    }
  temp->next = malloc(sizeof(struct strList));
  temp->next->funcName = strdup(str);
  temp->next->next = NULL;
  temp->next->offset = temp->offset+8;
}

void printStrings(FILE* fptr)
{
  struct strList* temp = stringCodeList;
  while(temp != NULL)
    {
      fprintf(fptr,"%s\n",temp->funcName);
      temp = temp->next;
    }
  printf("\n");

}

void addToStringRegion(char* val)
{
  addString(val);
}

int findOffset(char* s)
{
  struct strList* temp = stringCodeList;
  while(temp != NULL)
    {
      if(!strcmp(s,temp->funcName))
	{
	  return temp->offset;
	}
      temp = temp->next;
    }
  return -1;
}
