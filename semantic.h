#ifndef SEMANTIC_H
#define SEMANTIC_H

#include "main.h"
#include "hashTable.h"
#include "LinkedList.h"
#include "productionRules.h"
#include "hashTable.h"

/* Starts Sementic Analysis 
   Called in Main
*/
void semantic();

struct ScopeList* GetScope(char* scopename);
struct tokenHashList* typeOf(struct tokenTree* placement, struct ScopeList* CurrScope);
int SizeOf(struct ParameterList*);

extern struct TreeList* TreeListHead;
extern struct ScopeList* ScopeListHead;
extern int typeCheckIsOn;
#endif
