#ifndef LINKEDLIST_H
#define LINKEDLIST_H


#include "main.h"
#include "hashTable.h"
#include "tree.h"

/*List for the trees of each file*/
struct TreeList{
  struct TreeList* next;
  struct tokenTree* treeHead;
};


/*List for each scope*/
struct ScopeList{
  int structType;
  int scopeSize;
  struct ScopeList* next;
  struct table* tableVal[Size];
  char *scopeName;
  struct ParameterList* head;
  /* Add this functionality across all files */
};

extern struct ScopeList* ScopeListHead;
extern struct TreeList* TreeListHead;
extern struct tokenTree* head;
extern char* Lfilename;
extern char lastid[256];
extern int lineNumber;

void printScopesParameters();


void printFormattedScopeList();

int addToLinkedList();
int printFormattedLinkedList();

struct ScopeList* ScopeInit();
struct ScopeList* addNewScope(char* scopeName,int structType);

#endif
