%option noinput
%option nounput
%option yylineno

/* Should be noted that this was taken online */
BLOCKCOMMENTS           "/*"((\*+[^/*])|([^*]))*\**"*/" 
I                       ("0"|[1-9][0-9]*)
II                      0x[0-9A-Fa-f]+
III                     0[0-7]+ 
INTEGERLITERAL          {I}|{II}|{III}
D                       [0-9]
L                       [a-zA-Z_]
H                       [a-fA-F0-9]
O                       [0-7]
EE                      \[abfnrtv\\'\"]
RUNELITERAL             ('\\U{H}{H}{H}{H}{H}{H}{H}{H}')|('\\u{H}{H}{H}{H}')|('\\x{H}{H}')|'\\{O}{O}{O}'|'.'|'{EE}'

E                       [Ee][+-]?{D}+
FS                      (f|F|l|L)
IS                      (u|U|l|L)
W                       [ \t\f]*
LIT                     \"(\\.|[^\\"])*\"
IDENTIFIER              [a-zA-Z_][a-zA-Z0-9_]*
FLOATLITERAL            ({D}+{E}{FS}?)|({D}*"."{D}+({E})?{FS}?)|({D}+"."{D}*({E})?{FS}?)
IMAGINARY               ({INTEGERLITERAL}|{FLOATLITERAL})"i"


%{

#include "cbison.tab.h"
#include "tree.h"
extern FILE* yyin;
int comment();
void yyToGlobal(int );
extern int lineNumber;
extern char lastid[256];
extern char* Lfilename;
int ValidSemiColonPrevID = 0;
%}

%%

"//".*                  {//yyToGlobal(LIGNORE); return LIGNORE;
                        }

{BLOCKCOMMENTS}         {//yyToGlobal(LIGNORE); return LIGNORE;
                        }

{RUNELITERAL}           {yyToGlobal(LLITERAL); return LLITERAL;}


^{W}#.*\n               { ayyerror("g0 has no #preprocessor direcives. Are you feeding it C/C++ code?",1); }

\n                      { 
                            if(ValidSemiColonPrevID){
                                 yyToGlobal(LSM);return LSM;
                            }
                        }
[ \t\f]+                { //yyToGlobal(LIGNORE);return LIGNORE;
                        }

"struct"                { yyToGlobal(LSTRUCT);return LSTRUCT; }
"var"                   { yyToGlobal(LVAR);return LVAR; }
"const"                 { yyToGlobal(LCONST);return LCONST; }
"bool"                  { yyToGlobal(LBOOLEAN);return LBOOLEAN; }
"break"                 { yyToGlobal(LBREAK);ayyerror("Go keyword not in VGo",1); }
"func"                  { yyToGlobal(LFUNC);return LFUNC; }
"continue"              { yyToGlobal(LCONTINUE);ayyerror("Go keyword not in VGo",1); }
"float64"               { yyToGlobal(LFLOAT);return LFLOAT; }
"else"                  { yyToGlobal(LELSE);return LELSE; }
"for"                   { yyToGlobal(LFOR);return LFOR; }
"if"                    { yyToGlobal(LIF);return LIF; }
"int"                   { yyToGlobal(LINT);return LINT; }
"list"                  { yyToGlobal(LLIST);return LLIST; }
"return"                { yyToGlobal(LRETURN);return LRETURN; }
"string"                { yyToGlobal(LSTRING);return LSTRING;}
"table"                 { yyToGlobal(LTABLE);return LTABLE; }
"true"                  { yyToGlobal(LLITERAL);return LLITERAL; }
"false"                 { yyToGlobal(LLITERAL);return LLITERAL; }
"import"                { yyToGlobal(LIMPORT);return LIMPORT; }
"package"               { yyToGlobal(LPACKAGE);return LPACKAGE; }
"map"                   { yyToGlobal(LMAP);return LMAP; }
"type"                  { yyToGlobal(LTYPE);return LTYPE; }
"default"               { yyToGlobal(LDEFAULT);ayyerror("Go keyword not in VGo",1);}
"interface"             { yyToGlobal(LINTERFACE);ayyerror("Go keyword not in VGo",1);}
"select"                { yyToGlobal(LSELECT);ayyerror("Go keyword not in VGo",1);}
"case"                  { yyToGlobal(LCASE);ayyerror("Go keyword not in VGo",1);}
"defer"                 { yyToGlobal(LDEFER);ayyerror("Go keyword not in VGo",1);}
"go"                    { yyToGlobal(LGO);ayyerror("Go keyword not in VGo",1);}
"chan"                  { yyToGlobal(LCHAN);ayyerror("Go keyword not in VGo",1);}
"goto"                  { yyToGlobal(LGOTO);ayyerror("Go keyword not in VGo",1);}
"switch"                { yyToGlobal(LSWITCH);ayyerror("Go keyword not in VGo",1);}
"fallthrough"           { yyToGlobal(LFALL);ayyerror("Go keyword not in VGo",1);}
"range"                 { yyToGlobal(LRANGE);ayyerror("Go keyword not in VGo",1);}
"nil"                   { yyToGlobal(LNIL); return(LNIL);}

{IDENTIFIER}            { yyToGlobal(LIDENTIFIER);return LIDENTIFIER; }

{INTEGERLITERAL}        { yyToGlobal(LLITERAL);return LLITERAL; }

{FLOATLITERAL}          { yyToGlobal(LLITERAL);return LLITERAL; }

{LIT}                   { yyToGlobal(LLITERAL);return LLITERAL; }

{IMAGINARY}             { yyToGlobal(LLITERAL);return LLITERAL; }

<<EOF>>  {
     if(ValidSemiColonPrevID){
         yyToGlobal(LSM);return LSM;
         }
     else
         {
          yyterminate();
         }   
      }


"$"                     { yyToGlobal(LDOLLAR); ayyerror("Go operator not in VGo",1);}
"@"                     { yyToGlobal(LAT); ayyerror("Go operator not in VGo",1);}
"?"                     { yyToGlobal(LQUEST); ayyerror("Go operator not in VGo",1);}
"("                     { yyToGlobal(LLPAR);return LLPAR; }
")"                     { yyToGlobal(LRPAR);return LRPAR; }
"{"                     { yyToGlobal(LLBRA);return LLBRA; }
"}"                     { yyToGlobal(LRBRA);return LRBRA; }
"["                     { yyToGlobal(LLBRK);return LLBRK; }
"]"                     { yyToGlobal(LRBRK);return LRBRK; }
";"                     { yyToGlobal(LSM);return LSM; }
","                     { yyToGlobal(LCM);return LCM; }
"."                     { yyToGlobal(LDOT);return LDOT; }
"="                     { yyToGlobal(LASOP);return LASOP; }
"<"                     { yyToGlobal(LLT);return LLT; }
">"                     { yyToGlobal(LGT);return LGT; }
"!"                     { yyToGlobal(LEXC);return LEXC; }
":"                     { yyToGlobal(LCOLON);ayyerror("Go operator not in VGo",1);}
"~"                     { yyToGlobal(LCURLY);ayyerror("Go operator not in VGo",1);}
"=="                    { yyToGlobal(LEQ);return LEQ; }
"!="                    { yyToGlobal(LNE);return LNE; }
"<="                    { yyToGlobal(LLE);return LLE; }
">="                    { yyToGlobal(LGE);return LGE; }
"&&"                    { yyToGlobal(LANDAND);return LANDAND; }
"||"                    { yyToGlobal(LOROR);return LOROR; }
"++"                    { yyToGlobal(LINC);return LINC; }
"--"                    { yyToGlobal(LDEC);return LDEC; }
"+"                     { yyToGlobal(LPLUS);return LPLUS; }
"-"                     { yyToGlobal(LMINUS);return LMINUS; }
"*"                     { yyToGlobal(LSTAR);return LSTAR; }
"/"                     { yyToGlobal(LDIVIDE);return LDIVIDE; }
"&"                     { yyToGlobal(LAND);ayyerror("Go operator not in VGo",1); }
"|"                     { yyToGlobal(LOR);ayyerror("Go operator not in VGo",1); }
"^"                     { yyToGlobal(LNOT);ayyerror("Go operator not in VGo",1); }
"%"                     { yyToGlobal(LMOD);return LMOD; }
"<<"                    { yyToGlobal(LLSH);ayyerror("Go operator not in VGo",1); }
">>"                    { yyToGlobal(LRSH);ayyerror("Go operator not in VGo",1); }
"+="                    { yyToGlobal(LPLUSEQ);return LPLUSEQ; }
"-="                    { yyToGlobal(LMINUSEQ);return LMINUSEQ; }
"*="                    { yyToGlobal(LSTAREQ);ayyerror("Go operator not in VGo",1); }
"/="                    { yyToGlobal(LDIVEQ);ayyerror("Go operator not in VGo",1);}
"&="                    { yyToGlobal(LANDEQ);ayyerror("Go operator not in VGo",1); }
"|="                    { yyToGlobal(LOREQ);ayyerror("Go operator not in VGo",1); }
"^="                    { yyToGlobal(LNOTEQ);ayyerror("Go operator not in VGo",1); }
"%="                    { yyToGlobal(LMODEQ);ayyerror("Go operator not in VGo",1); }
"<<="                   { yyToGlobal(LLLE);ayyerror("Go operator not in VGo",1);}
">>="                   { yyToGlobal(LRRE);ayyerror("Go operator not in VGo",1);}
":="                    { yyToGlobal(LCOLAS);ayyerror("Go operator not in VGo",1); }
"..."                   { yyToGlobal(LDDD);ayyerror("Go operator not in VGo",1); }
"&^"                    { yyToGlobal(LANDNOT);ayyerror("Go operator not in VGo",1); }
"<-"                    { yyToGlobal(LCOMM);ayyerror("Go operator not in VGo",1); }
"&^="                   { yyToGlobal(LANDNOTEQ);ayyerror("Go operator not in VGo",1);}
.                       { ayyerror("lexical error",1); }

%%

int yychar;
int errors;

int ayyerror(const char *s,int status)
{
   errors++;

   fprintf(stderr, "%s: ", s);

   if (Lfilename)
      fprintf(stderr, "file \"%s\", ", Lfilename );

   fprintf(stderr, "line %d, token = \"%s\"\n", yylineno, yytext);
   exit(status);
}

int yyerror(const char *s)
{
   errors++;

   fprintf(stderr, "%s: ", s);

   if (Lfilename)
      fprintf(stderr, "file \"%s\", ", Lfilename );

   fprintf(stderr, "line %d, token = \"%s\"\n", yylineno, yytext);
   exit(2);
}

/*
#ifndef yywrap

 int yywrap(){
     return -1;
 }

#endif
*/

int lex_sync()
{
    /* Probably because of its use for interactive line-interpreters
     * like "dc", original yacc uses a "lazy" lookahead, that is to say, it
     * does not fetch a lookahead when the only action is the default
     * reduction. But our scanner-feedback must keep the lookahead in
     * sync. This routine sees to it that the lookahead has been
     * fetched.
     *
     * yychar is the yacc lookahead token. It is -1 when
     * yacc is being "lazy". yylex() is allowed to return -1 (or any
     * negative int) to indicate EOF, but yacc uses 0 to indicate EOF.
*/

if(yychar == -1)
if((yychar = yylex()) < 0)
yychar = 0;
return 0;
}


void yyToGlobal(int yylexReturn)
{
    if(debug)
    {
       printf("DEBUG: returning %d %s\n", yylexReturn, yytext);
    }
    if(yylexReturn == LSM)
    {
       strcpy(lastid,";");
    }
    else
    {
       strcpy(lastid,yytext);
    }
lineNumber = yylineno;
if(yylexReturn == LLITERAL || yylexReturn == LIDENTIFIER || yylexReturn == LBREAK || yylexReturn == LCONTINUE || yylexReturn == LFALL || yylexReturn == LRETURN
	       || yylexReturn == LINC || yylexReturn == LDEC || yylexReturn == LRPAR || yylexReturn == LRBRA || yylexReturn == LRBRK || yylexReturn == LINT || yylexReturn == LBOOLEAN || yylexReturn == LSTRING || yylexReturn == LFLOAT )
               ValidSemiColonPrevID = 1;
else
               ValidSemiColonPrevID = 0;

    if(yylexReturn == 0)
	    return;
    if(yylexReturn == LIGNORE)
	    return;
    yylval.node = AllocateTreeLeafNode(yylexReturn);
    return;

}
