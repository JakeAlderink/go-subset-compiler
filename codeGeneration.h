#ifndef CODEGEN_H
#define CODEGEN_H

#include "main.h"
#include "tree.h"
#include "LinkedList.h"
#include "semantic.h"
extern struct tokenTree* head;
extern struct ScopeList* ScopeListHead;
extern char* Lfilename;

struct threeAddress
{
  int region;
  int offset;
};

struct codeList
{
  int opcode;
  struct codeList* next;
  struct threeAddress* destination; 
  struct threeAddress* source; 
  struct threeAddress* source1; 
};




struct funcList {
  struct funcList* next;
  char* funcName;
};

struct strList {
  struct strList* next;
  char* funcName;
  int offset;
};


struct priorityList{
  struct priorityList* next;
  int labelVal;
};



struct codeList* codeHead;  
struct priorityList* priorityListHead;
struct funcList* funcListHead;
struct strList* stringCodeList;

int stringRegionByteSize;
/*NOTE MOST OF THIS DERIVES FROM DR. J's WEBSITE*/

#define GLOBAL_REGION 2001 /* can assemble as relative to the pc */
#define LOCAL_REGION  2002 /* can assemble as relative to the ebp */
#define LABEL  2004        /* pseudo-region for labels in the code region */
#define CONST_REGION  2005 /* pseudo-region for immediate mode constants */
#define STRUCT_REGION 2006
#define PARAM_REGION  2007
#define STRING_REGION 2008

#define USTAR_OP 2998
#define USUB_OP  2999
#define UADD_OP  3000
#define ADD_OP   3001
#define SUB_OP   3002
#define MUL_OP   3003
#define DIV_OP   3004

#define ASN_OP   3006

#define LCONT_OP 3008
#define SCONT_OP 3009

#define BLT_OP   3011
#define BLE_OP   3012
#define BGT_OP   3013
#define BGE_OP   3014
#define BEQ_OP   3015
#define BNE_OP   3016
#define BIF_OP   3017
#define BNIF_OP  3018
#define PARM_OP  3019
#define CALL_OP  3020
#define RET_OP   3021
#define MOD_OP   3022
#define GOTO_OP  3023

#define GLOB_DEC  3051
#define PROC_DEC  3052
#define LOCAL_DEC 3053
#define LABEL_DEC 3054
#define END_DEC   3055


void addToStringRegion(char* val);
void generateCode();
struct codeList* genCodeInstance(int, struct threeAddress*, struct threeAddress*, struct threeAddress*);
struct threeAddress* newTemp();
struct threeAddress* newLabel();
struct codeList* concat(struct codeList*,struct codeList*);
char* getFuncName(struct funcList* curr,int index);
void addFuncName(char* name);

void addString(char* str);
void printStrings();

int getFuncIndex(struct funcList* curr, char* name);
int getFuncParamSize(struct ScopeList* curr);
char* myStrCat(char* s,char* t);
void addToList(int val);
int isIn(int val);
int findOffset(char*s);
#endif

